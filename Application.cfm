<cfprocessingdirective pageencoding="utf-8">

<cfapplication name="Portal_EAD_DESENV2" sessionmanagement="Yes" sessiontimeout=#CreateTimeSpan(0,2,0,0)#>

<cfscript>
	// Mapeamento pasta Testbox
	this.mappings[ "/testbox" ] = expandPath( "D:/Repositorios/Ficha_Inscricao_EAD/testbox/" );

	//Configuração da aplicação
	this.securejson=false;
	this.sessionmanagement = true;
	this.setclientcookies = true;
	this.clientManagement = true;
	this.applicationtimeout="#createtimespan(1,0,0,0)#";
	SetLocale("Portuguese (Brazilian)");

	// Data Sources
	application.datasource = 'ds_portal_ead_admin'; // Banco temporario

	// Data source Ficha Tecnica eg_ficha_tecnica@sspro.sp.senac.br
	application.datasource_ficha_tecnica = 'ds_ficha_tecnica';

	// Data source Senac Solution appl$ss_integracao@cs90snc
	application.datasource_appl_ss_integracao = 'ds_appl_ss_integracao';

	//application.cfc_mapping = 'admin.cfc';

	// Verifica o protocolo http
	if(CGI.HTTPS eq "off" or CGI.HTTPS eq '') 
		httpProtocolo = 'http://';
	else
	  	httpProtocolo = 'https://';  

	// Configurações do admin
	Application.AdminTitulo = 'Admin - Portal EAD';
	Application.AdminPathLogico = httpProtocolo & CGI.HTTP_HOST & '/admin';
	Application.PathCFC         = 'admin.cfc';



	// Intragração Portal EAD
	Application.API_portalEAD.endpoint = 'https://api.homead.sp.senac.br';
	Application.API_portalEAD.usuario = 'DR.SP';
	Application.API_portalEAD.senha = 'senachomol';

	//Integração Base Única EAD
	Application.API_BaseUnica_EAD.endpoint = 'https://api.homead.sp.senac.br/baseunica';

</cfscript>