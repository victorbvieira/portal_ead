<cfprocessingdirective pageencoding="utf-8" >
<cfinclude template="./../templates/check_autenticao.cfm" >	
<cfscript>
	v_area   = 'oferta';
	v_pagina = 'oferta';

	//Navegação
	v_dsc_area = 'Oferta';
	v_dsc_sub_area = 'Sincronizar Oferta';
	v_titulo_form = 'Informações da Oferta';

	//Controle Barra Ações
	url.BtnNovo		= false;
	url.BtnEditar	= false;
	url.BtnExcluir	= false;
	
	cfcPagina = CreateObject("component","#Application.PathCFC#.#v_area#");

	v_dados = cfcPagina.f_getByID(p_id = url.id);
		
	// Setup dos valores do form
	v_form_action = 'view/delete'; // valores: insert, view/delete, update
	
	//Setando Variaveis
	session.id				= v_dados.id; 
	v_frm_id				= v_dados.id; //obrigatorio
	v_frm_cod_ficha			= v_dados.cod_ft; //obrigatorio
	v_frm_sigla				= v_dados.sigla;
	v_frm_dt_inicio			= v_dados.dt_inicio; //obrigatorio
	v_frm_dt_termino		= v_dados.dt_fim; //obrigatorio
	v_frm_dt_publicacao		= ''; //obrigatorio
	v_frm_dt_expiracao		= '';
	v_frm_preco_normal		= ''; //obrigatorio
	v_frm_preco_promocional	= '';
	v_frm_investimento		= ''; //obrigatorio
	v_frm_forma_pagamento	= ''; //obrigatorio
	v_frm_qtd_parcelas		= ''; 
	v_frm_polos				= '';

	// informativo
	v_frm_acad_career		= v_dados.acad_carrer;
	v_frm_strm				= v_dados.strm;
	v_frm_acad_prog			= v_dados.acad_prog;
	
</cfscript>


<!DOCTYPE html>
<html lang="pt">

	<cfinclude template="../templates/page_head.cfm" >

<body style="">
<cfoutput>
<section id="container">
	<!--- header --->
	<cfinclude template="../templates/header.cfm" >

	<!--- Menu Lateral --->
	<cfinclude template="../templates/menu.cfm" >

	<!--- Conteudo Principal --->
    <section id="main-content" class="">
		<section class="wrapper">
        
        <!--- Sub Header --->
        <div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb small">
                	<li><a href="#Application.AdminPathLogico#/home.cfm"><i class="fa fa-home"></i> Home</a></li>
                	<li>Administração</li>
                    <li><a href="#Application.AdminPathLogico#/operadores/operadores.cfm">Operadores</a></li>
                    <li class="active">View</li>
				</ul>
                    
			</div>
		</div>
         
        
        <cfinclude template="./../templates/barra_acoes.cfm" > 
         

		<form action="./../cfc/#v_area#.cfc?method=SincronizarToPortalEAD" method="post" enctype="multipart/form-data" name="form_1" id="form_1">
     		<cfinclude template="./#v_pagina#_form.cfm">
     		<input type="hidden" name="frm_retorno" value="#Application.AdminPathLogico#/#v_area#/#v_pagina#.cfm">
     		<input type="hidden" name="cod_oferta" value="#v_frm_id#">

			<br>
			<div class="panel-footer">
				<a href="#v_pagina#.cfm"><button type="button" class="btn "><i class="fa fa-remove"></i> Voltar</button></a>
				<a href="javascript:document.getElementById('form_1').submit();"><button type="button" class="btn btn-primary bluebtn right"><i class="fa fa-save"></i> &nbsp; Sincronizar </button></a>
			</div>
		</form>
			
		</section>
	</section>
</section>

<div class="clearfix"> </div>
	
<cfinclude template="./../templates/rodape.cfm" >

</cfoutput>			

</body>
</html>

<cfinclude template="../templates/footer_page.cfm" >

