<cfscript>
	switch(v_form_action) {
		case "insert":
			v_config_input = '';
			break;
		case "update":
			v_config_input = '';
			break;
		case "view/delete":
			v_config_input = 'readonly style="border: 0px solid white;"';
			break;
	}
</cfscript>


<cfoutput>
	
<!--- Informações de Cadastro --->	
<div class="row">	
	<div class="col-sm-12">
		<section class="panel panel-default">
			<header class="panel-heading">
				<i class="fa fa-list-alt"></i> #v_titulo_form#
				<span class="tools pull-right">
				<a href="javascript:;" class="fa fa-chevron-down"></a>
				<!--- <a href="javascript:;" class="fa fa-times"></a> Fecha --->
			</span>
		</header>
		
		<div class="panel-body">
			
			<table border="0" width="100%">
				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Cód. Oferta: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete' or v_form_action eq 'insert'>
							<div class="panel-body">#v_frm_id#</div>
						<cfelse>
							<input type="text" name="frm_id" value="#v_frm_acad_career#" class="form-control input-sm" #v_config_input# readonly/>
						</cfif>
					</td>
				</tr>
				
				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Carreira: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_acad_career#</div>
						<cfelse>
							<input type="text" name="frm_acad_career" value="#v_frm_acad_career#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Programa: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_acad_prog#</div>
						<cfelse>
							<input type="text" name="frm_acad_prog" value="#v_frm_acad_prog#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Período Letivo: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_strm#</div>
						<cfelse>
							<input type="text" name="frm_strm" value="#v_frm_strm#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Cód. Ficha Técnica: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_cod_ficha#</div>
						<cfelse>
							<input type="text" name="frm_cod_ficha" value="#v_frm_cod_ficha#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Sigla Oferta: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_sigla#</div>
						<cfelse>
							<input type="text" name="frm_sigla" value="#v_frm_sigla#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Data início: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_dt_inicio#</div>
						<cfelse>
							<input type="text" name="frm_dt_inicio" value="#v_frm_dt_inicio#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Data Término: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_dt_termino#</div>
						<cfelse>
							<input type="text" name="frm_dt_termino" value="#v_frm_dt_termino#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Data Publicação: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_dt_publicacao#</div>
						<cfelse>
							<input type="text" name="frm_dt_publicacao" value="#v_frm_dt_publicacao#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Data Expiração: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_dt_expiracao#</div>
						<cfelse>
							<input type="text" name="frm_dt_expiracao" value="#v_frm_dt_expiracao#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Preço Normal: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_preco_normal#</div>
						<cfelse>
							<input type="text" name="frm_preco_normal" value="#v_frm_preco_normal#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Preço Promocional: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_preco_promocional#</div>
						<cfelse>
							<input type="text" name="frm_preco_promocional" value="#v_frm_preco_promocional#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Investimento: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_investimento#</div>
						<cfelse>
							<input type="text" name="frm_investimento" value="#v_frm_investimento#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Forma de Pagamento: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_forma_pagamento#</div>
						<cfelse>
							<input type="text" name="frm_forma_pagamento" value="#v_frm_forma_pagamento#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Qtd. Parcelas: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_qtd_parcelas#</div>
						<cfelse>
							<input type="text" name="frm_qtd_parcelas" value="#v_frm_qtd_parcelas#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" >
					<td width="200px">
						<label class="control-label">&nbsp;Polos: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_polos#</div>
						<cfelse>
							<input type="text" name="frm_polos" value="#v_frm_polos#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

			</table>
		</div>
	</div>
</div>
</cfoutput>