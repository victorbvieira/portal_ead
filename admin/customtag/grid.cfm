<!--- **********************************************************************************
Nome:   Grid
Versão: 3.0
Funçãoo: Retorna uma lista com os registros paginados.
Data:   08/08/2016
Autor:  Victor Borring Vieira

Parametros:
	Titulo  				--> Titulo da Lista 
	RecordsPerPage			--> Numero de registros que apareceram por pagina
	*Query  				--> ResultSet com os dados que ser�o apresentados
	OrdemColunas			--> Este parametro recebe uma string, com os campos que ser�o apresentados no grid separado por ',' ex. CODALUNO,NOME,RA
	MsgNullRegistros		--> Define a frase que sera apresentada caso n�o existam registros no data set
	SetURLDestino			--> Defini para onde deve ir quando clicar sobre um registro.	

********************************************************************************** --->


<!--- ******************** Tratamento dos parametros ************************ --->

<cfprocessingdirective pageEncoding="utf-8">

<cfparam name = "Attributes.titulo" 				default = "&nbsp;">
<cfparam name = "Attributes.RecordsPerPage" 		default = "10">
<cfparam name = "Attributes.SetChave"			    default = "">
<cfparam name = "Attributes.SetURLDestino"			default = "">
<cfparam name = "Attributes.MsgNullRegistros"		default = "Nenhum registro encontrado">
<cfparam name = "Attributes.vSincronizacao"	default = "false">



<!--- Abortar se não tiver este parametro --->
<cfif NOT IsDefined("Attributes.query")>
  <strong>Error O pametro Query  obrigatório</strong>
  <cfabort>
</cfif>

<cfparam name="Attributes.OrdemColunas"		default="#Attributes.query.ColumnList#">
<cfif #ArrayLen(listToArray(Attributes.OrdemColunas))# neq #ArrayLen(listToArray(Attributes.query.ColumnList))#>
	<strong>Erro: A Lista de colunas deve conter os mesmos registro que a query</strong>
	<cfabort>
</cfif>


<cfset Attributes.OrdemColunas = "#listToArray(Attributes.OrdemColunas)#">


<!---  **************************************************** 
Tratar quando não deve existir pagina��o

<cfif IsDefined('RegFull')>
	<cfset pagesetup = 999999999>
<cfelse>
	<cfset pagesetup = #Application.paginacao#>
</cfif>
--->


<!---  **************************************************** --->
<cfparam name="Page" default="1">

<cfset TotalRecords 	= "#Attributes.query.RecordCount#">
<cfset FirstRecord  	= "#((Attributes.RecordsPerPage*Page)-Attributes.RecordsPerPage)+1#">
<cfset LastRecord		= "#Attributes.RecordsPerPage*Page#">
<cfset PreviousPage		= "#Page-1#">
<cfset NextPage			= "#Page+1#">
<cfset TotalPages		= "#Ceiling(TotalRecords/Attributes.RecordsPerPage)#">

<cfscript>     
	function stripHTML(str) {          
		return REReplaceNoCase(str,"<[^>]*>","","ALL");     }
</cfscript>

<!---  **************************************************** --->
<cfoutput>
	
<div class="col-lg-12">

	<div class="panel panel-default filterable"> 
		<div class="panel-heading">
			<i class="fa fa-edit lblue"></i> #Attributes.titulo# 
			<div class="pull-right"> 
				<!---<button class="btn btn-default btn-xs btn-filter mright5">
  					<span class="glyphicon glyphicon-filter size14"></span>
				</button>--->
			</div>
		</div>
		<div class="panel-body container-fluid offset-0"> 

			<div class="table-responsive mtop-18">
				<table class="table table-bordered table-hover table-striped">
					<!--- Cabeçalho --->
					<thead>
						<tr class="filters">
							<cfloop index="x" from="1" to="#arrayLen(Attributes.OrdemColunas)#">
								<th class="small"><input type="text" class="form-control" placeholder="#ReplaceNoCase(Attributes.OrdemColunas[x],'_',' ','ALL')#" disabled></th>
							</cfloop>
						</tr>
					</thead>
				
					<!--- Loop Resultado --->
					<tbody>
						<cfloop query="Attributes.query" startrow="#FirstRecord#" endrow="#LastRecord#">
							<tr>
								<cfloop index="x" from="1" to="#arrayLen(Attributes.OrdemColunas)#">
									<td class="tab_lista_resultado">
										#MID(Evaluate(Attributes.OrdemColunas[x]),1,50)#
									</td>
								</cfloop>
								<!--- Link 
								 <cfif Attributes.SetURLDestino NEQ 'false'>
              onClick="location.href='#Attributes.SetURLDestino#&id=id<cfif Isdefined('cod')>&id2=#cod#</cfif><cfif Isdefined('DATA_EFETIVA')>&id3=#DATA_EFETIVA#</cfif><cfif Isdefined('gerencia')>&id_gerencia=#gerencia#</cfif><cfif Isdefined('ano')>&id2=#ano#</cfif><cfif Isdefined('mes')>&mes=#mes#</cfif>&vSincronizacao=#Attributes.vSincronizacao#';"
			  </cfif>
								--->
								<td class="tab_lista_resultado center"> <a href="#Attributes.SetURLDestino#&#Attributes.SetChave#=#Evaluate(Attributes.SetChave)#" title="Visualizar"> <i class="fa fa-eye dark size18"></i> </a></td>
							</tr>
            			</cfloop>
					</tbody>
				</table>
			</div>  
		</div>
	</div>
</div>
			

<!--- Paginação --->
<div class="center small grey">FORAM LOCALIZADOS #TotalRecords# REGISTROS</div>
<div class="clearfix"> </div>

<cfif TotalRecords neq 0>
	<div class="col-md-6 mtop15">
		<i class="fa fa-list-alt"></i>
		Mostrando <span class="lblue">#FirstRecord#</span> a <span class="lblue"><cfif LastRecord gt TotalRecords> #TotalRecords#<cfelse> #LastRecord#</cfif></span> de <b> #TotalRecords# resultados </b>
	</div>



	<div class="col-md-6">
		<nav>
	    	<ul class="pagination right">
	    		<!--- Pagina Anterior --->
	    		<cfif PreviousPage neq 0>
	    			<cfabort>
	            	<cfif IsDefined('Keyword')>
	            		<li>
	            			<a onClick="location.href='?area=URL.area&arquivo=URL.arquivo&btn_novo=false&page=#PreviousPage#&keyword=#Keyword#&field=#field#&id=id';" aria-label="Previous">
	                    		<span aria-hidden="true">«</span>
	                  		</a>
	                  	</li>
	              	<cfelse>
	              		<li>
	              			<a onClick="location.href='?area=URL.area&arquivo=URL.arquivo&btn_novo=false&page=#PreviousPage#&id=id';" aria-label="Previous">
	                    		<span aria-hidden="true">«</span>
	                  		</a>
	              		</li>
	            	</cfif>
	            <cfelse>
	            	<li class="disabled">
		            	<a aria-label="Previous">
		            		<span aria-hidden="true">«</span>
		          		</a>
					</li>
	          	</cfif>

	          	
	          	<!--- Paginas Numeradas --->
	          	<cfif Attributes.query.recordcount gt 0>
					<cfif TotalPages gt 1>
	                	<cfset pag_ini = "#Page - 5#">
	                	<cfif pag_ini LT 1 >
	                  		<cfset pag_ini = "1">
	                	</cfif>e
	                	<cfset pag_fim = "#Page + 5#">
	                	<cfif pag_fim GT "#TotalPages#">
	                  		<cfset pag_fim = "#TotalPages#">
	                	</cfif>i
	                	<cfif pag_ini GT 1>
	                  		<cfif pag_ini neq 2>
	                    		<li class="disabled">
			                    	..
			                    </li>
			                <cfelse>
				                <li>
			                    	<a href="?area=URL.area&arquivo=URL.arquivo&btn_novo=false&page=1&id=id">1 </a>
			                    </li>
	                  		</cfif>
	                	</cfif>
	                	
						<cfloop index="Page2Go" from="#pag_ini#" to="#pag_fim#">
	                  		<cfif IsDefined('Keyword')>
	                    		<cfif Page2Go eq Page>
	                    			<li class="active">
				                    	<a >#Page# <span class="sr-only">(current)</span></a>
				                    </li>
	                      		<cfelse>
	                      			<a href="?area=URL.area&arquivo=URL.arquivo&btn_novo=false&page=#Page2Go#&keyword=#Keyword#&field=#field#&id=id"> #Page2Go# </a>
	                    		</cfif>
	                    	<cfelse>
	                    		<cfif Page2Go eq Page>
	                      			<li class="active">
				                    	<a>#Page# <span class="sr-only">(current)</span></a>
				                    </li>
	                      		<cfelse>
	                      			<li>
				                    	<a href="?area=URL.area&arquivo=URL.arquivo&btn_novo=false&page=#Page2Go#&id=id">#Page2Go# <span class="sr-only">(current)</span></a>
				                    </li>
	                    		</cfif>
	                  		</cfif>
	                	</cfloop>
	                	
		                <cfif pag_fim LT #TotalPages#>
		                  <cfif pag_fim neq #TotalPages# - 1>
		                  	<li class="disabled">
		                    	..
		                    </li>
		                  </cfif>
		                  	<li>
		                    	<a href="?area=URL.area&arquivo=URL.arquivo&btn_novo=true&page=#TotalPages#&id=id">#TotalPages# </a>
		                    </li>
		                </cfif>
	              </cfif>

	            </cfif>
	          	
	            <!--- proxima pagina --->
	            <cfif val(NextPage) lte val(TotalPages)>
	            	<cfif IsDefined('Keyword')>
	            		<li>
	                    	<a onClick="location.href='?area=URL.area&arquivo=URL.arquivo&btn_novo=false&page=#NextPage#&keyword=#keyword#&field=#field#&id=id';" aria-label="Next"><span aria-hidden="true">»</span></a>
	                    </li>
	              	<cfelse>
	              		<li>
	                    	<a onClick="location.href='?area=URL.area&arquivo=URL.arquivo&btn_novo=false&page=#NextPage#&id=id';" aria-label="Next"><span aria-hidden="true">»</span></a>
	                    </li>
	            	</cfif>
	            <cfelse>
	            	<li class="disabled">
	                  	<a aria-label="Next"><span aria-hidden="true">»</span></a>
	                </li>
				</cfif>
	        </ul>
		</nav>
	</div>
</cfif>
</cfoutput>