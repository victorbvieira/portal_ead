<cfprocessingdirective pageencoding="utf-8" >

<cfscript>
	v_msg = '';
	
	if (isDefined('form.frm_email')){
		cfcOperadores = CreateObject("component","#Application.PathCFC#.operadores");
		
		v_autentica = cfcOperadores.autentica(form.frm_email, form.frm_senha); 
		if (v_autentica.recordcount eq 1){
			session.id_user = v_autentica.id;
			location(url="#Application.AdminPathLogico#/home.cfm") 
		}else{
			v_msg = 'E-mail e/ou Senha inválidos.';	
		}		
	}
	
</cfscript>

<cfoutput><span style="color:##f4f4f4;">#CGI.LOCAL_ADDR#</span></cfoutput>

<!DOCTYPE html>
<html lang="pt">
	<cfinclude template="templates/page_head.cfm" >
	  
	<body class="login-body">
		<div class="container">

      		<form class="form-signin" action="index.cfm" method="post" >
        		<h2 class="form-signin-heading">LOGIN <b><cfoutput>#Application.AdminTitulo#</cfoutput></b></h2>
       			<div class="login-wrap">
            		<div class="user-login-info">
            			<cfif v_msg neq ''>
            				<span style="color:red"><cfoutput>#v_msg#</cfoutput></span>
            				<br><br>
            			</cfif>
            			E-mail
                		<input type="text" name="frm_email" class="form-control" placeholder="E-mail" autofocus>
                		Senha
                		<input type="password" name="frm_senha" class="form-control" placeholder="Senha">
            		</div>
                    <button class="btn btn-lg btn-login btn-block" type="submit">Entrar</button>
				</div>
			</form>
		</div>
	</body>
</html>	