<cfprocessingdirective pageencoding="utf-8" >
<cfinclude template="./../templates/check_autenticao.cfm" >	
<cfscript>
	v_area   = 'ficha_tecnica';
	v_pagina = 'ficha_tecnica';

	//Navegação
	v_dsc_area = 'Ficha Técnica';
	v_dsc_sub_area = 'Sincronizar Ficha Técnica';
	v_titulo_form = 'Informações da Ficha Técnica';

	//Controle Barra Ações
	url.BtnNovo		= false;
	url.BtnEditar	= false;
	url.BtnExcluir	= false;
	
	cfcPagina = CreateObject("component","#Application.PathCFC#.#v_area#");

	v_dados = cfcPagina.f_getByID(p_id = url.id);
		
	// Setup dos valores do form
	v_form_action = 'view/delete'; // valores: insert, view/delete, update
	
	//Setando Variaveis
	session.id				= v_dados.id;
	v_frm_cod_ficha			= v_dados.id;
	v_frm_data_efetiva		= v_dados.data_efetiva;
	v_frm_titulo			= v_dados.titulo;
	v_frm_gerencia			= v_dados.gerencia;
	v_frm_area				= v_dados.area;
	v_frm_subarea			= v_dados.subarea;
	v_frm_modalidade		= v_dados.modalidade;
	v_frm_cargaHoraria		= v_dados.cargaHoraria;
	v_frm_descricao			= v_dados.descricao;
	v_frm_preRequisitos		= v_dados.preRequisitos;
	v_frm_publicoAlvo		= v_dados.publicoAlvo;
	v_frm_metodologia		= v_dados.metodologia;
	v_frm_progama			= v_dados.progama;
	v_frm_mercadoTrabalho	= v_dados.mercadoTrabalho;
	v_frm_documentacao		= v_dados.documentacao;
	v_frm_certificacao		= v_dados.certificacao;
	v_frm_autorizacao		= v_dados.autorizacao;
	v_frm_material_didatico	= v_dados.material_didatico;
	v_frm_tags				= v_dados.tags;
</cfscript>


<!DOCTYPE html>
<html lang="pt">

	<cfinclude template="../templates/page_head.cfm" >

<body style="">
<cfoutput>
<section id="container">
	<!--- header --->
	<cfinclude template="../templates/header.cfm" >

	<!--- Menu Lateral --->
	<cfinclude template="../templates/menu.cfm" >

	<!--- Conteudo Principal --->
    <section id="main-content" class="">
		<section class="wrapper">
        
        <!--- Sub Header --->
        <div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb small">
                	<li><a href="#Application.AdminPathLogico#/home.cfm"><i class="fa fa-home"></i> Home</a></li>
                	<li>Administração</li>
                    <li><a href="#Application.AdminPathLogico#/operadores/operadores.cfm">Operadores</a></li>
                    <li class="active">View</li>
				</ul>
                    
			</div>
		</div>
         
        
        <cfinclude template="./../templates/barra_acoes.cfm" > 
         

		<form action="./../cfc/#v_area#.cfc?method=SincronizarToPortalEAD" method="post" enctype="multipart/form-data" name="form_1" id="form_1">
     		<cfinclude template="./#v_pagina#_form.cfm">
     		<input type="hidden" name="frm_retorno" value="#Application.AdminPathLogico#/#v_area#/#v_pagina#.cfm">
     		<input type="hidden" name="cod_ficha" value="#v_frm_cod_ficha#">

			<br>
			<div class="panel-footer">
				<a href="#v_pagina#.cfm"><button type="button" class="btn "><i class="fa fa-remove"></i> Voltar</button></a>
				<a href="javascript:document.getElementById('form_1').submit();"><button type="button" class="btn btn-primary bluebtn right"><i class="fa fa-save"></i> &nbsp; Sincronizar </button></a>
			</div>
		</form>
			
		</section>
	</section>
</section>

<div class="clearfix"> </div>
	
<cfinclude template="./../templates/rodape.cfm" >

</cfoutput>			

</body>
</html>

<cfinclude template="../templates/footer_page.cfm" >

