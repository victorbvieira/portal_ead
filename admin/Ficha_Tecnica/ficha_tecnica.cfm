<cfprocessingdirective pageencoding="utf-8" >
<cfinclude template="./../templates/check_autenticao.cfm" >		
<cfscript>
	v_area   = 'ficha_tecnica';
	v_pagina = 'ficha_tecnica';

	//Navegação
	v_dsc_area = 'Ficha Técnica';
	v_dsc_sub_area = 'Sincronizar Ficha Técnica';

	//Controle Barra Ações
	url.BtnNovo		= false;
	url.BtnEditar	= false;
	url.BtnExcluir	= false;
	
	cfcPagina = CreateObject("component","#Application.PathCFC#.#v_area#");
	
	v_frm_id 		= '';
	v_frm_titulo 	= '';
	
	if (isDefined('form.frm_acao')){
		if (isDefined('form.frm_id'))
			v_frm_id = form.frm_id;
		
		if (isDefined('form.frm_titulo'))
			v_frm_titulo = form.frm_titulo;
		
		//Necessario para a paginação	
		session.form_frm_id 			= v_frm_id;
		session.form_frm_titulo			= v_frm_titulo;
		
		v_dados = cfcPagina.f_consulta(p_id 	= v_frm_id
									  ,p_titulo	= v_frm_titulo);	
									  
	}else{
		if (isDefined('url.page')){
			//Necessario para a paginação	
			v_dados = cfcPagina.f_consulta(p_id 	= session.form_frm_id
										  ,p_titulo	= session.form_frm_titulo);
										  
			v_frm_id 		= session.form_frm_id;
			v_frm_titulo	= session.form_frm_titulo;
		}	
	}
</cfscript>


<!DOCTYPE html>
<html lang="pt">

	<cfinclude template="../templates/page_head.cfm" >

<body style="">
<cfoutput>
<section id="container">
	<!--- header --->
	<cfinclude template="../templates/header.cfm" >

	<!--- Menu Lateral --->
	<cfinclude template="../templates/menu.cfm" >

	<!--- Conteudo Principal --->
    <section id="main-content" class="">
		<section class="wrapper">
        
        <!--- Sub Header --->
        <div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb small">
                	<li><a href="#Application.AdminPathLogico#/home.cfm"><i class="fa fa-home"></i> Home</a></li>
                	<li>#v_dsc_area#</li>
                    <li><a href="#Application.AdminPathLogico#/#v_area#/#v_pagina#.cfm">#v_dsc_sub_area#</a></li>
                    <li class="active">Index</li>
				</ul> 
			</div>
		</div>
        
        <cfinclude template="./../templates/barra_acoes.cfm" > 
        
        <!--- Mensagem de alerta --->
		<cfinclude template="./../templates/mensagem_alerta.cfm" > 
         
		<!--- Caixa de pesquisa --->
		<div class="row ">  
    		<div class="col-lg-12 ">
				<section class="panel panel-default ">
            		<header class="panel-heading pesquisas" id="accordion">
						<div class="col-md-3  titulopainel"><i class="fa fa-search "></i> PESQUISAR </div>
	                	<div class="col-md-7 center mt2">
	
							<form class="form-inline" method="post" action="#v_pagina#.cfm">
					        	<div class="form-group">
									<label class="size11" style="margin-right:0;" for="pref-search">Cód. Ficha Técnica:</label>
					                <div class="input-group ">
					                	<input type="number" class="form-control" name="frm_id" value="#v_frm_id#">
			                            <span class="input-group-btn">
			                                <button class="btn btn-success" type="submit"><i class="fa fa-search-plus" aria-hidden="true"></i></button>
			                            </span>
			                            <input type="hidden" name="frm_acao" value="pesquisa">
			                        </div>
			                    </div>
							</form> 
	
						</div><!-- form group [search] -->
	            				
	            		<div class="col-md-2 padding5">
	            			<button type="button" class="btn-block btn btn-primary size11 accordion-toggle" data-toggle="collapse" data-parent="##accordion" href="##collapseOne">
	               				Pesquisa Avançada
	           				</button>
	           			</div> 
       				</header>
       
       				<div id="collapseOne" class="panel-collapse collapse ">
        				<div class="panel-body"  >

            				<form class="form-horizontal bucket-form"  method="post" action="#v_area#.cfm">
            					<input type="hidden" name="frm_acao" value="pesquisa">
                					
                				<div class="form-group ">
                    				<label class="col-sm-3 control-label">Cód. Ficha Técnica: </label>
                    				<div class="col-sm-6">
                        				<input type="number" class="form-control input-sm" name="frm_id" value="#v_frm_id#">
                    				</div>
                				</div>
                				
                				<div class="form-group ">
                    				<label class="col-sm-3 control-label">Título: </label>
                    				<div class="col-sm-6">
                        				<input type="text" class="form-control input-sm" name="frm_titulo" value="#v_frm_titulo#">
                    				</div>
                				</div>
					                
								<div class="form-group">
									<label class="col-sm-3 control-label"></label>
									<div class="col-sm-6">
  										<button type="submit" class="btn btn-primary bluebtn  btn-block" data-toggle="collapse" data-target="##collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Procurar</button> 
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
		<!--- Caixa de pesquisa FIM --->


		<!--- Grid --->
		<cfif isDefined('v_dados')>
			<div class="row">
			  <cfmodule template="./../customtag/grid.cfm" 
	              titulo			= "#v_dsc_area#"
	              query				= "#v_dados#"
	              RecordsPerPage	= "8" 
	              SetChave			= "ID"
	              ShowColunaID		= false
	              OrdemColunas		= "ID, data_efetiva, titulo"
	              SetURLDestino		= "#v_pagina#_view.cfm?"/>
			</div>	
		</cfif>

		</section>
	</section>
</section>


<div class="clearfix"> </div>
	
<cfinclude template="./../templates/rodape.cfm" >

</cfoutput>			

</body>
</html>

<cfinclude template="../templates/footer_page.cfm" >

