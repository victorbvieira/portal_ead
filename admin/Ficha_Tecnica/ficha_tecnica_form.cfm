<cfscript>
	switch(v_form_action) {
		case "insert":
			v_config_input = '';
			break;
		case "update":
			v_config_input = '';
			break;
		case "view/delete":
			v_config_input = 'readonly style="border: 0px solid white;"';
			break;
	}
</cfscript>


<cfoutput>
	
<!--- Informações de Cadastro --->	
<div class="row">	
	<div class="col-sm-12">
		<section class="panel panel-default">
			<header class="panel-heading">
				<i class="fa fa-list-alt"></i> #v_titulo_form#
				<span class="tools pull-right">
				<a href="javascript:;" class="fa fa-chevron-down"></a>
				<!--- <a href="javascript:;" class="fa fa-times"></a> Fecha --->
			</span>
		</header>
		
		<div class="panel-body">
			
			<table border="0" width="100%">
				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Cód. Ficha Técnica: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete' or v_form_action eq 'insert'>
							<div class="panel-body">#v_frm_cod_ficha#</div>
						<cfelse>
							<input type="text" name="frm_cod_ficha" value="#v_frm_cod_ficha#" class="form-control input-sm" #v_config_input# readonly/>
						</cfif>
					</td>
				</tr>
				
				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Data Efetiva: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_data_efetiva#</div>
						<cfelse>
							<input type="text" name="frm_data_efetiva" value="#v_frm_data_efetiva#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Título: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_titulo#</div>
						<cfelse>
							<input type="text" name="frm_titulo" value="#frm_titulo#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Área: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_area#</div>
						<cfelse>
							<input type="text" name="frm_area" value="#v_frm_area#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Sub-Área: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_subarea#</div>
						<cfelse>
							<input type="text" name="frm_subarea" value="#v_frm_subarea#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Modalidade: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_modalidade#</div>
						<cfelse>
							<input type="text" name="frm_modalidade" value="#v_frm_modalidade#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Carga Horária: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_cargaHoraria#</div>
						<cfelse>
							<input type="text" name="frm_cargaHoraria" value="#v_frm_cargaHoraria#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Descrição: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_descricao#</div>
						<cfelse>
							<input type="text" name="frm_descricao" value="#v_frm_descricao#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Pré Requisitos: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_preRequisitos#</div>
						<cfelse>
							<input type="text" name="frm_preRequisitos" value="#v_frm_preRequisitos#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Público Alvo: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_publicoAlvo#</div>
						<cfelse>
							<input type="text" name="frm_publicoAlvo" value="#v_frm_publicoAlvo#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Metodologia: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_metodologia#</div>
						<cfelse>
							<input type="text" name="frm_metodologia" value="#v_frm_metodologia#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Programa: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_progama#</div>
						<cfelse>
							<input type="text" name="frm_progama" value="#v_frm_progama#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Mercado de Trabalho: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_mercadoTrabalho#</div>
						<cfelse>
							<input type="text" name="frm_mercadoTrabalho" value="#v_frm_mercadoTrabalho#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Documentação: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_documentacao#</div>
						<cfelse>
							<input type="text" name="frm_documentacao" value="#v_frm_documentacao#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Certificação: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_certificacao#</div>
						<cfelse>
							<input type="text" name="frm_certificacao" value="#v_frm_certificacao#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Autorização: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_autorizacao#</div>
						<cfelse>
							<input type="text" name="frm_autorizacao" value="#v_frm_autorizacao#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Material Didatico: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_material_didatico#</div>
						<cfelse>
							<input type="text" name="frm_material_didatico" value="#v_frm_material_didatico#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Tags: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_tags#</div>
						<cfelse>
							<input type="text" name="frm_tags" value="#v_frm_tags#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
</cfoutput>