<cfprocessingdirective pageencoding="utf-8" >

<cfinclude template="templates/check_autenticao.cfm" >

<!DOCTYPE html>
<html lang="pt">

	<cfinclude template="templates/page_head.cfm" >

<body style="">

<cfoutput>
<section id="container">
	<!--- header --->
	<cfinclude template="templates/header.cfm" >

	<!--- Menu Lateral --->
	<cfinclude template="templates/menu.cfm" >


	<!--- Conteudo Principal --->
    <section id="main-content" class="">
		<section class="wrapper">
        
        <!--- Sub Header --->
        <div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb small">
                	<li><a href="#Application.AdminPathLogico#/home.cfm"><i class="fa fa-home"></i> Home</a></li>
				</ul>
                    
			</div>
		</div>

        <div class="row">
			<div class="col-md-12">
				<section class="panel  panel-default ">
	            	<div class="panel-body" >
						&nbsp; Senac São Paulo
						
					</div>
				</section>
			</div>
        </div>
        
		</section>
	</section>
</section>

<div class="clearfix"> </div>
	
<cfinclude template="./templates/rodape.cfm" >

</cfoutput>

</body>
</html>

<cfinclude template="templates/footer_page.cfm" >
