<cfprocessingdirective pageencoding="utf-8" >
<cfscript>
	vBarAcaoFechada = true;
	
	if (vBarAcaoFechada){
		vBarAcao_aux1 = 'up';
		vBarAcao_aux2 = 'none';
	}else{
		vBarAcao_aux1 = 'down';
		vBarAcao_aux2 = 'block';
	}
	
	vBtnNovo 	= false;
	vBtnEditar 	= false;
	vBtnExcluir = false;
	
	if(isDefined('url.BtnEditar'))
		vBtnEditar = url.BtnEditar;
		
	if(isDefined('url.BtnNovo'))
		vBtnNovo = url.BtnNovo;
		
	if(isDefined('url.BtnExcluir'))
		vBtnExcluir = url.BtnExcluir;

	if(!isDefined('v_area'))
		v_area = '';
	
	if(!isDefined('v_pagina'))
		v_pagina = '';
	
	v_url_base = Application.AdminPathLogico & '/' & v_area & '/' & v_pagina;
	
	try{
		v_parametros = mid(CGI.http_url, find('?', CGI.http_url), len(CGI.http_url) - find('?', CGI.http_url) + 1);	
	} catch (any e){
		v_parametros = '';
	}

	if (v_parametros eq '')
	v_parametros = '?' & cgi.QUERY_STRING;
	
	
	
	v_url_novo		= v_url_base & '_insert.cfm' & v_parametros;
	v_url_editar 	= v_url_base & '_update.cfm' & v_parametros; 
	v_url_excluir	= v_url_base & '_delete.cfm' & v_parametros; 
</cfscript>

<cfoutput>
<div class="row">
	<div class="col-md-12">
    	<section class="panel  panel-default ">
        	<header class="panel-heading">
            	<i class="fa fa-gear"></i> AÇÕES
                <span class="tools pull-right">
                	<a href="javascript:;" class="fa fa-chevron-#vBarAcao_aux1#"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
				</span>
			</header>

            <div class="panel-body" style="display: #vBarAcao_aux2#" >
            	
            	<div class="col-md-2 col-xs-6 padding5">
            		<cfif vBtnNovo>
            			<a href="#v_url_novo#">
            				<button type="button" class="btn btn-default col-md-12 col-xs-12"><i class="fa fa-plus"></i> Novo</button>
            			</a>
            		<cfelse>
            			<button data-toggle="button" class="btn btn-white disabled col-md-12 col-xs-12"><i class="fa fa-plus"></i> Novo</button>
            		</cfif>
            	</div>
            	
            	<div class="col-md-2 col-xs-6 padding5">
            		<cfif vBtnEditar>
            			<a href="#v_url_editar#">
            				<button type="button" class="btn btn-default col-md-12 col-xs-12"><i class="fa fa-edit"></i> Editar</button>
            			</a>
            		<cfelse>
            			<button data-toggle="button" class="btn btn-white disabled col-md-12 col-xs-12"><i class="fa fa-edit"></i> Editar</button>
            		</cfif>
            	</div>
            	
            	<div class="col-md-2 col-xs-6 padding5">
            		<cfif vBtnExcluir>
            			<a href="#v_url_excluir#">
            				<button type="button" class="btn btn-default col-md-12 col-xs-12"><i class="fa fa-remove"></i> Excluir</button>
            			</a>
            		<cfelse>
            			<button data-toggle="button" class="btn btn-white disabled col-md-12 col-xs-12"><i class="fa fa-remove"></i> Excluir</button>
            		</cfif>
            	</div>
			</div>
		</section>
	</div>
</div>
</cfoutput>