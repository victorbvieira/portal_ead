<cfprocessingdirective pageencoding="utf-8" >

<cfoutput>
<header class="header fixed-top clearfix">
	<!--- Logo --->
	<div class="brand">
		<a href="#Application.AdminPathLogico#/home.cfm" class="navbar-brand logo"><img src="#Application.AdminPathLogico#/img/logo.png" alt=""></a>
        	<div class="sidebar-toggle-box">
        		<div class="fa fa-bars"></div>
    		</div>
	</div>
	
	<!--- Titulo --->
	<div class="nav notify-row" id="top_menu" >
    	<span class="dark">
			<i class="fa fa-desktop"></i>
			<b>App Senac</b> 
   			<br>
   			<small>Gerênciamento e Controle do App</small>
		</span>
	</div>

</header>
</cfoutput>