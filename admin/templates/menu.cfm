<cfprocessingdirective pageencoding="utf-8" >
<cfoutput>
<aside>
	<div id="sidebar" class="nav-collapse " >
    	<!-- sidebar menu start-->           
        <div class="leftside-navigation" tabindex="5000">  
		
			<ul class="sidebar-menu" id="nav-accordion">
            	<!--- Ficha Tecnica --->
                <li class="sub-menu dcjq-parent-li">
                    <a href="javascript:;" class="dcjq-parent"> 
                        <i class="fa fa-files-o"/></i>
                        <span>FICHA TÉCNICA</span>
                        <span class="dcjq-icon"></span>
                    </a>
                    <ul class="sub" style="display: none;">
                        <li><a href="#Application.AdminPathLogico#/ficha_tecnica/ficha_tecnica.cfm">SINCRONIZAR FICHA TÉCNICA</a></li>
                    </ul>
				</li>
				
				<!--- Ficha Tecnica --->
                <li class="sub-menu dcjq-parent-li">
                    <a href="javascript:;" class="dcjq-parent"> 
                        <i class="fa fa-files-o"/></i>
                        <span>OFERTA</span>
                        <span class="dcjq-icon"></span>
                    </a>
                    <ul class="sub" style="display: none;">
                        <li><a href="#Application.AdminPathLogico#/oferta/oferta.cfm">SINCRONIZAR OFERTA</a></li>
                    </ul>
                </li>


        		<!--- DE/Para --->
                <li class="sub-menu dcjq-parent-li">
                	<a href="javascript:;" class="dcjq-parent"> 
                    	<i class="fa fa-files-o"/></i>
                    	<span>DE/PARA</span>
                		<span class="dcjq-icon"></span>
                	</a>
	                <ul class="sub" style="display: none;">
                        <li><a href="#Application.AdminPathLogico#/depara_area/depara_area.cfm">ÁREA</a></li>
                        <li><a href="#Application.AdminPathLogico#/depara_modalidade/depara_modalidade.cfm">MODALIDADE</a></li>
	                </ul>
                </li>
                
                <!--- Administração --->
       			<li class="sub-menu dcjq-parent-li">
                	<a href="javascript:;" class="dcjq-parent"> 
                    	<i class="fa fa-files-o"/></i>
                    	<span>ADMINISTRAÇÃO</span>
                		<span class="dcjq-icon"></span>
                	</a>
	                <ul class="sub" style="display: none;">
	                    <li><a href="#Application.AdminPathLogico#/operadores/operadores.cfm">OPERADORES</a></li>
	                </ul>
            	</li>
			</ul>            	
		</div>        
	</div>
</aside>  

</cfoutput>          	
            	
            	
<!---		Exemplos          	
            	

            	<li class="sub-menu dcjq-parent-li">
                	<a href="javascript:;" class="dcjq-parent">
                    	<i class="fa fa-files-o"></i>
                    	<span>ATIV. EXTENSIVAS</span>
                		<span class="dcjq-icon"></span></a>
	                <ul class="sub" style="display: none;">
	                    <li><a href="#">AÇÕES CURTA DURAÇÃO</a></li>
	                </ul>
            	</li>

           		<li class="sub-menu dcjq-parent-li">
                	<a href="javascript:;" class="dcjq-parent">
                		<i class="fa fa-file-text"></i>
                    	<span>EXTENSÃO</span>
                		<span class="dcjq-icon"></span></a>
                	<ul class="sub" style="display: none;">
                    	<li class="sub-menu dcjq-parent-li">
                        <a href="javascript:;" class="dcjq-parent">
                    <span>TERCEIRO NÍVEL</span>
                <span class="dcjq-icon"></span></a>
                <ul class="sub sub2" style="display: none;">
                    <li class="sub-menu dcjq-parent-li"><a href="#">EAD</a></li>
                    <li><a href="#">PRES. INTEGRADO</a></li>
                    <li><a href="#">PRES. NÃO INTEGRADO</a></li> 
                </ul>
                    </li>
                    <li><a href="#">PRES. INTEGRADO</a></li>
                    <li><a href="#">PRES. NÃO INTEGRADO</a></li>
                </ul>
            </li>
      
            <li class="sub-menu dcjq-parent-li">
             <a href="javascript:;" class="dcjq-parent">
                <i class="fa fa-file-text"></i>
                    <span>CURSOS TÉCNICOS</span>
               </a>
               </li>
                <li class="sub-menu dcjq-parent-li">
             <a href="javascript:;" class="dcjq-parent">
                <i class="fa fa-file-text"></i>
                    <span>QUALIF. PROFISSIONAL</span>
               </a>
--->   