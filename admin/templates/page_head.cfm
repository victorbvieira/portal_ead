<cfprocessingdirective pageencoding="utf-8" >
<cfoutput>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content=""> 

	<title>Admin - Matrícula</title>

	<!-- Bootstrap -->
	<link href="#Application.AdminPathLogico#/css/bootstrap.min.css" rel="stylesheet">

	<!-- Icones Fonts -->
	<link href="#Application.AdminPathLogico#/css/font-awesome.min.css" rel="stylesheet" type="text/css">  

	<!-- Font -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,300,800' rel='stylesheet' type='text/css'>  

	<!-- Custom CSS -->
	<link href="#Application.AdminPathLogico#/css/custom_admin.css" rel="stylesheet"> 
	<!-- Custom styles for this template -->
    <link href="#Application.AdminPathLogico#/css/style.css" rel="stylesheet">
    <link href="#Application.AdminPathLogico#/css/style-responsive.css" rel="stylesheet"> 


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> 
        <![endif]-->
</head>
</cfoutput>