<cfprocessingdirective pageencoding="utf-8" >
<cfoutput>
<cfif isDefined('url.msg')>
	<div class="panel panel-default filterable"> 
		<div class="panel-heading">
			<i class="fa fa-edit lblue"></i> ATENÇÃO
		</div>
		<div class="panel-body container-fluid offset-0"> 
			<div class="table-responsive mtop-18" style="color:red">
				<br>&nbsp;&nbsp;&nbsp;&nbsp;#url.msg#
            </div>
        </div>
	</div>  	
</cfif>	
</cfoutput>