<cfoutput>
	
<!-- Placed js at the end of the document so the pages load faster -->
<!-- jQuery -->
<script src="#Application.AdminPathLogico#/js/jquery-1.12.0.js"></script> 

<!-- Bootstrap Core JavaScript -->
<script src="#Application.AdminPathLogico#/js/bootstrap.min.js"></script>

<!-- JavaScript Custom para Todas as Páginas-->
<script src="#Application.AdminPathLogico#/js/scripts_custom.js"></script> 

<!--common script init for all pages-->
<script src="#Application.AdminPathLogico#/js/plugins/scripts.js"></script>


<script src="#Application.AdminPathLogico#/js/plugins/jquery.nicescroll.js"></script>


<script class="include" type="text/javascript" src="#Application.AdminPathLogico#/js/plugins/jquery.dcjqaccordion.2.7.js"></script>

</cfoutput>