<cfscript>
	switch(v_form_action) {
		case "insert":
			v_config_input = '';
			break;
		case "update":
			v_config_input = '';
			break;
		case "view/delete":
			v_config_input = 'readonly style="border: 0px solid white;"';
			break;
	}
</cfscript>


<cfoutput>
	
<!--- Informações de Cadastro --->	
<div class="row">	
	<div class="col-sm-12">
		<section class="panel panel-default">
			<header class="panel-heading">
				<i class="fa fa-list-alt"></i> Informações de Cadastro
				<span class="tools pull-right">
				<a href="javascript:;" class="fa fa-chevron-down"></a>
				<!--- <a href="javascript:;" class="fa fa-times"></a> Fecha --->
			</span>
		</header>
		
		<div class="panel-body">
			
			<table border="0" width="100%">
				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;ID: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete' or v_form_action eq 'insert'>
							<div class="panel-body">#v_frm_id#</div>
						<cfelse>
							<input type="text" name="frm_id2" value="#v_frm_id#" class="form-control input-sm" #v_config_input# readonly/>
						</cfif>
					</td>
				</tr>
				
				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;ID Modalidade: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_id_modalidade#</div>
						<cfelse>
							<input type="text" name="frm_id_modalidade" value="#v_frm_id_modalidade#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
				
				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Modalidade: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_modalidade#</div>
						<cfelse>
							<input type="text" name="frm_modalidade" value="#v_frm_modalidade#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
				
				<tr height="40px" >
					<td width="200px">
						<label class="control-label">&nbsp;ID Modalidade SS: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_id_ss#</div>
						<cfelse>
							<input type="text" name="frm_id_ss" value="#v_frm_id_ss#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>

				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Sigla: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_sigla#</div>
						<cfelse>
							<input type="text" name="frm_sigla" value="#v_frm_sigla#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
</cfoutput>