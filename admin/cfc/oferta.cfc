<cfcomponent>
	<cfprocessingdirective pageEncoding='utf-8'>
	
	<cffunction name="f_consulta" returntype="query"  access="remote" >
		<cfargument name="p_id" 			type="any" displayname="Cód Ficha Tecnica"	required="no">
		<cfargument name="p_titulo"			type="any" displayname="Título"				required="no">
		
		<cfquery name="qry" datasource="#Application.datasource_appl_ss_integracao#" maxrows=100>
			select A.DIG_CODIGO_OFERTA as ID,
				   A.DIG_SIGLA as SIGLA,
				   a.ACAD_CAREER as MODALIDADE,
				   A.STRM as PERIODO_LETIVO
			  FROM sysadm.PS_DIG_OFE_CURSO A
				INNER JOIN sysadm.PS_DIG_OFE_PER_AUL B
					ON (A.DIG_CODIGO_OFERTA = B.DIG_CODIGO_OFERTA)
				INNER JOIN sysadm.PS_ACAD_PROG_TBL C
					ON (A.ACAD_PROG = C.ACAD_PROG)
				WHERE C.EFFDT = (SELECT MAX(C_ED.EFFDT)
									FROM sysadm.PS_ACAD_PROG_TBL C_ED
								WHERE C_ED.ACAD_PROG = C.ACAD_PROG
									AND C_ED.EFFDT <= SYSDATE)
			 <cfif isDefined('arguments.p_id') and arguments.p_id neq ''>
			 	and A.DIG_CODIGO_OFERTA = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
			 </cfif>
			 <cfif isDefined('arguments.p_titulo') and arguments.p_titulo neq ''>
			 	and upper(A.DIG_SIGLA) like upper(<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.p_titulo#%">)
			 </cfif>
			 order by A.DIG_SIGLA 
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	<cffunction name="f_getByID" returntype="query"  access="remote" >
		<cfargument name="p_id" 		type="numeric" displayname="Cód Ficha Tecnica" required="yes">
		
		<cfquery name="qry" datasource="#Application.datasource_appl_ss_integracao#">
			SELECT  A.DIG_CODIGO_OFERTA  as id,
					A.DIG_SIGLA          as sigla,
					a.ACAD_CAREER        as acad_carrer,
					A.STRM               as strm,
					A.ACAD_PROG          as acad_prog,
					C.DESCR				 as desc_prog,
					C.DESCRSHORT         as cod_ft,
					A.CAMPUS             as campus,
					B.START_DATE         as dt_inicio,
					B.END_DATE           as dt_fim
				FROM sysadm.PS_DIG_OFE_CURSO A
				INNER JOIN sysadm.PS_DIG_OFE_PER_AUL B
					ON (A.DIG_CODIGO_OFERTA = B.DIG_CODIGO_OFERTA)
				INNER JOIN sysadm.PS_ACAD_PROG_TBL C
					ON (A.ACAD_PROG = C.ACAD_PROG)
				WHERE C.EFFDT = (SELECT MAX(C_ED.EFFDT)
									FROM sysadm.PS_ACAD_PROG_TBL C_ED
								WHERE C_ED.ACAD_PROG = C.ACAD_PROG
									AND C_ED.EFFDT <= SYSDATE)
				AND A.DIG_CODIGO_OFERTA = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	
	<cffunction name="SincronizarToPortalEAD" access="remote" >
		<cfscript>
			v_sincronizacao = this.SincronizarToPortalEAD_Control(p_id = form.cod_oferta);
			v_destino = form.frm_retorno & '?msg=#v_msg_retorno#';
			
			location(url = "#v_destino#");
    	</cfscript>
	</cffunction>

	<cffunction name="SincronizarToPortalEAD_Control" access="remote" >
		<cfargument name="p_id"	type="numeric" displayname="Cód Turma" required="yes">

		<cfscript>
			cfcAPI_PortalEAD = CreateObject("component","#Application.PathCFC#.API_Portal_EAD");
			cfcDeParaModalidade = CreateObject("component","#Application.PathCFC#.depara_modalidade");
			cfcDeParaArea = CreateObject("component","#Application.PathCFC#.depara_area");

			v_dados_oferta = this.f_getByID(p_id = arguments.p_id);
			
			// tratamento do dados de entrada
			v_idCursoOrigem 	= v_dados_oferta.cod_ft;//*obrigatorio, Id do curso que será vinculado a turma, validação Integridade referencial
			v_idTurmaOrigem		= v_dados_oferta.id;//*obrigatorio, Identificador único da turma no departamento de origem exemplo PK
			
			v_dataInicio		= dateConvert("local2utc", v_dados_oferta.dt_inicio);//Define quando a turma inicia no curso
			v_dataInicio		= dateFormat( v_dataInicio, "yyyy-mm-dd" ) & "T" & timeFormat( v_dataInicio, "HH:mm:ss" ) & "Z";
			
			v_dataTermino		= v_dados_oferta.dt_fim;//Define quando a turma termina o curso
			v_dataPublicacao	= '2018-05-18T00:00:00Z';//*obrigatorio, Define a data de início para realização da inscrição
			v_dataExpiracao		= '2018-05-18T00:00:00Z';//*obrigatorio, Define a data de termino para realização da inscrição
			v_precoNormal		= '99999999';//*obrigatorio, Representa o valor da parcela do curso
			v_precoPromocional	= '';//Representa o valor do curso com desconto
			v_investimento		= ' ';//*obrigatorio, Texto com informações de investimentos e condições de pagamento
			v_formasPagamento	= ' ';//*obrigatorio, Texto curto com informações sobre a forma de pagamento (Exemplo Boleto bancário e cartão de credito)
			v_quantidadeParcelas= '';//Campo informativo que será usado para representar quantidade de parcelas do valor do curso(promocional ou normal)
			v_polos				= '';//CNPJ dos polos que estão disponíveis no processo deinscrição
			
			v_retorno_api = cfcAPI_PortalEAD.f_set_turma(p_idCursoOrigem	= v_idCursoOrigem
														,p_idTurmaOrigem	= v_idTurmaOrigem
														,p_dataInicio		= v_dataInicio
														,p_dataTermino		= v_dataTermino
														,p_dataPublicacao	= v_dataPublicacao
														,p_dataExpiracao	= v_dataExpiracao
														,p_precoNormal		= v_precoNormal
														,p_precoPromocional	= v_precoPromocional
														,p_investimento		= v_investimento
														,p_formasPagamento	= v_formasPagamento
														,p_quantidadeParcelas = v_quantidadeParcelas
														,p_polos			= v_polos);

			if (v_retorno_api.statuscode eq '201 Created'){
				v_msg_retorno_api = 'Registro criado com sucesso';
				v_statuscode_retorno_api = v_retorno_api.statuscode;
			}
		
			//Caso o Registro já exista chamar metodo de atualização
			if (v_retorno_api.statuscode eq '400 Bad Request' and v_retorno_api.retorno.detail eq 'A turma informado ja existe. - Id da turma duplicado para o DR'){
				v_retorno_api = cfcAPI_PortalEAD.f_put_turma(p_idCursoOrigem	= v_idCursoOrigem
															,p_idTurmaOrigem	= v_idTurmaOrigem
															,p_dataInicio		= v_dataInicio
															,p_dataTermino		= v_dataTermino
															,p_dataPublicacao	= v_dataPublicacao
															,p_dataExpiracao	= v_dataExpiracao
															,p_precoNormal		= v_precoNormal
															,p_precoPromocional	= v_precoPromocional
															,p_investimento		= v_investimento
															,p_formasPagamento	= v_formasPagamento
															,p_quantidadeParcelas = v_quantidadeParcelas
															,p_polos			= v_polos);

				if (v_retorno_api.statuscode eq '200 OK'){
					v_msg_retorno_api = 'Registro Atualizado com sucesso';
					v_statuscode_retorno_api = v_retorno_api.statuscode;
				}
			}

			if (v_retorno_api.statuscode neq '201 Created' and v_retorno_api.statuscode neq '200 OK'){
				v_msg_retorno_api = 'Erro ao processar o registro<br>';
				v_msg_retorno_api &= v_retorno_api.retorno.message & '<br>';
				v_msg_retorno_api &= #v_retorno_api.retorno.detail#;	
				v_statuscode_retorno_api = v_retorno_api.statuscode;	
			}

			v_msg_retorno = "Oferta: #arguments.p_id# Processada<br>";
			v_msg_retorno &= v_msg_retorno_api;

			v_retorno.mensage	= v_msg_retorno;
			v_retorno.statuscode	= v_statuscode_retorno_api;
			return v_retorno;
		</cfscript>
	</cffunction>

</cfcomponent>