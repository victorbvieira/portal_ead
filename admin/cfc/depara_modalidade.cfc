<cfcomponent>
	
	<cffunction name="f_consulta" returntype="query"  access="remote" >
		<cfargument name="p_id" 			    type="any" displayname="id"				        required="no">
		<cfargument name="p_id_modalidade"	    type="any" displayname="id modalidade"      	required="no">
		<cfargument name="p_modalidade" 		type="any" displayname="modalidade"	            required="no">
		<cfargument name="p_id_modalidade_ss" 	type="any" displayname="Id modalidade no SS"	required="no">
		
		<cfquery name="qry" datasource="#Application.datasource#">
			select ID,
			       ID_MODALIDADE,
				   MODALIDADE,
				   ID_MODALIDADE_SS
			  from DEPARA_PORTAL_EAD_MODALIDADE
			 where ind_ativo = 'A'
			 <cfif isDefined('arguments.p_id') and arguments.p_id neq ''>
			 	and ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
			 </cfif>
			 <cfif isDefined('arguments.p_id_modalidade') and arguments.p_id_modalidade neq ''>
			 	and ID_MODALIDADE = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id_modalidade#">
			 </cfif>
			 <cfif isDefined('arguments.p_modalidade') and arguments.p_modalidade neq ''>
			 	and upper(MODALIDADE) like upper(<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.p_modalidade#%">)
			 </cfif>
			 <cfif isDefined('arguments.p_id_ss') and arguments.p_id_ss neq ''>
			 	and ID_MODALIDADE_SS = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id_ss#">
			 </cfif>
			 
			 order by MODALIDADE 
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	<cffunction name="f_getByID" returntype="query"  access="remote" >
		<cfargument name="p_id" 		type="numeric" displayname="Sistema"		required="yes">
		
		<cfquery name="qry" datasource="#Application.datasource#">
			select *
			  from DEPARA_PORTAL_EAD_MODALIDADE
			 where id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
			   and ind_ativo = 'A'
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	
	<cffunction name="insert" access="remote" >
		
		<cfquery name="qry" datasource="#Application.datasource#">
			insert into DEPARA_PORTAL_EAD_MODALIDADE
				(id, id_modalidade, modalidade, sigla, id_modalidade_ss)
			values
			(
				SEQ_DEPARA_MODALIDADE.nextval,
				<cfqueryparam cfsqltype="cf_sql_integer" value="#form.frm_id_modalidade#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_modalidade#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_sigla#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#form.frm_id_ss#">
			)
		</cfquery>
		
		<cfscript>
    		v_destino = form.frm_retorno & '?msg=De/Para Cadastrado';
    	</cfscript>
    	
    	<cflocation url="#v_destino#" >
	</cffunction>
	
	<cffunction name="update" access="remote" >
		
		<cfquery name="qry" datasource="#Application.datasource#">
			update DEPARA_PORTAL_EAD_MODALIDADE
			   set id_modalidade 	= <cfqueryparam cfsqltype="cf_sql_integer" value="#form.frm_id_modalidade#">,
                   modalidade 		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_modalidade#">,
                   sigla	        = <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_sigla#">,
                   id_modalidade_ss = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.frm_id_ss#">,
				   dt_alt           = sysdate
			 where id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#form.frm_id#">
		</cfquery>
			
		<cfscript>
    		v_destino = form.frm_retorno & '?msg=De/Para Atualizado';
    	</cfscript>
    	
    	<cflocation url="#v_destino#" >
	</cffunction>

	<cffunction name="delete" access="remote" >
		
		<cfquery name="qry" datasource="#Application.datasource#">
			update DEPARA_PORTAL_EAD_MODALIDADE
			   set ind_ativo = 'I',
			   	   dt_alt = sysdate
			 where id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#form.frm_id#">
		</cfquery>
			
		<cfscript>
    		v_destino = form.frm_retorno & '?msg=De/Para Indativado';
    	</cfscript>
    	
    	<cflocation url="#v_destino#" >
	</cffunction>

	<cffunction name="f_getID_portalEADByID_SS" returntype="any"  access="remote" >
		<cfargument name="p_id_modalidade_ss" 		type="numeric" displayname="id modalidade SS"		required="yes">
		
		<cfquery name="qry" datasource="#Application.datasource#">
			select id_modalidade
			  from DEPARA_PORTAL_EAD_MODALIDADE
			 where id_modalidade_ss = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id_modalidade_ss#">
			   and ind_ativo = 'A'
		</cfquery>
		
		<cfreturn qry.id_modalidade>
	</cffunction>
</cfcomponent>