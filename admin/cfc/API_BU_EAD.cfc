<cfcomponent>
<cfoutput>

	<!---depara get
		depara put

		compra get
		compra put

		interesse get
		interesse put

		processoSeletivo get
		processoSeletivo put
	--->

	<cffunction name="f_get_pessoa" returntype="any"  access="remote" >
		<cfargument name="p_origem"			type="any" displayname="Código Da Origem" required="true"/>
		<cfargument name="p_codPessoaCorp"	type="any" displayname="Código Pessoa Na pessoa" required="true"/>
		
		<cfscript>
			//Recupera token
			v_token = CreateObject("component","#Application.PathCFC#.API_Portal_EAD").f_autentica().retorno.token;
		</cfscript>

		<cfhttp url="#Application.API_BaseUnica_EAD.endpoint#/pessoa/consultar/#arguments.p_origem#/#arguments.p_codPessoaCorp#"
        		method="get"
        		result="resultJSON"
        		charset="utf-8">

    		<cfhttpparam type="header" name="Accept" value="application/json">
    		<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
    		<cfhttpparam type="header" name="Authorization" value="jwt:#v_token#">
		</cfhttp>

		<cfscript>
			return resultJSON;
			v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
			v_retorno.Statuscode = resultJSON.Statuscode;
			//return v_retorno;
		</cfscript>
	</cffunction>


	<cffunction name="f_put_pessoa" returntype="any"  access="remote" >
		<cfargument name="p_codOrigemStatus"	type="any" displayname="Status do registro no sistema de origem" required="true"/>
		<cfargument name="p_dscOrigem"			type="any" displayname="Origem do DR que enviou o registro" required="true"/>
		<cfargument name="p_codPessoa"			type="any" displayname="Codigo de pessoa na origem (PK no DR)" required="true"/>
		<cfargument name="p_indTipoPessoa"		type="any" displayname="Pessoa Física ou Juridica (F ou J)" required="true"/>
		<cfargument name="p_nomPessoa"			type="any" displayname="Nome da Pessoa / Razão Social" required="true"/>
		<cfargument name="p_numCpfCnpj"			type="any" displayname="CPF para PF e CNPJ para PJ" required="true"/>
		<cfargument name="p_datNascto"			type="any" displayname="Data de Nascimento" required="true"/>
		<cfargument name="p_datOrigemAtual"	type="any" displayname="Data de atualização na origem" required="true"/>
		<cfargument name="p_datOrigemCadtro"	type="any" displayname="Data de cadastro na origem" required="true"/>
		<cfargument name="p_DscEmail"			type="any" displayname="Email" required="true"/>
		<cfargument name="p_DscEmailPartcl"			type="any" displayname="Email" required="true"/>
		
		
		<cfscript>
			//Recupera token
			v_token = CreateObject("component","#Application.PathCFC#.API_Portal_EAD").f_autentica().retorno.token;
		</cfscript>

		<cfsavecontent variable = "v_corpo">
			{
			    "CodOrigemStatus": "#arguments.p_codOrigemStatus#",
				"DscOrigem": "#arguments.p_dscOrigem#",
				"CodPessoa": "#arguments.p_codPessoa#",
				"IndTipoPessoa": "#arguments.p_indTipoPessoa#",
				"NomPessoa": "#arguments.p_nomPessoa#",
				"NumCpfCnpj": "#arguments.p_numCpfCnpj#",
				"DatNascto": "#arguments.p_datNascto#",
				"DatOrigemAtualz": "#arguments.p_datOrigemAtual#",
			    "DatOrigemCadtro": "#arguments.p_datOrigemCadtro#",
				"EMails":[{
					"DscEmail":"#arguments.p_DscEmailPartcl#",
					"Tipo":"Particular"
				}]
			}
		</cfsavecontent>

		

		<cfhttp url="#Application.API_BaseUnica_EAD.endpoint#/pessoa/gravar"
        		method="put"
        		result="resultJSON"
        		charset="utf-8">

    		<cfhttpparam type="header" name="Accept" value="application/json">
    		<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
			<cfhttpparam type="header" name="Authorization" value="jwt:#v_token#">
			<cfhttpparam type="body" name="jUser" value="#v_corpo#">
		</cfhttp>
		
		<cfscript>
			return resultJSON;
			v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
			v_retorno.Statuscode = resultJSON.Statuscode;
			//return v_retorno;
		</cfscript>
	</cffunction>


	<cffunction name="f_get_depara" returntype="any"  access="remote" >
		<cfargument name="p_origem"			type="any" displayname="Corresponde a origem do registro. Ex: DR_SP" required="true"/>
		<cfargument name="p_codigoPessoa"	type="any" displayname="Corresponde ao código da pessoa na base de origem" required="true"/>
		
		<cfscript>
			//Recupera token
			v_token = CreateObject("component","#Application.PathCFC#.API_Portal_EAD").f_autentica().retorno.token;
		</cfscript>

		<cfhttp url="#Application.API_BaseUnica_EAD.endpoint#/depara/consultar/#arguments.p_origem#/#arguments.p_codigoPessoa#"
        		method="get"
        		result="resultJSON"
        		charset="utf-8">

    		<cfhttpparam type="header" name="Accept" value="application/json">
    		<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
    		<cfhttpparam type="header" name="Authorization" value="jwt:#v_token#">
		</cfhttp>

		<cfscript>
			return resultJSON;
			v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
			v_retorno.Statuscode = resultJSON.Statuscode;
			//return v_retorno;
		</cfscript>
	</cffunction>

	<cffunction name="f_put_depara" returntype="any"  access="remote" >
		<cfargument name="p_origem"					type="any" displayname="Origem do DR que enviou o registro" required="true"/>
		<cfargument name="p_codigoPessoa"			type="any" displayname="Corresponde ao código da pessoa na base de origem." required="true"/>
		<cfargument name="p_codigoPessoaCorp"		type="any" displayname="Código Único da pessoa" required="true"/>
		<cfargument name="p_dataDeInclusao"			type="any" displayname="Data de inclusão do registro" required="true"/>
		<cfargument name="p_origemStatus"			type="any" displayname="Status do registro no sistema de origem" required="true"/>
		<cfargument name="p_dataOrigemAtualizacao"	type="any" displayname="Data de atualização do registro na origem" required="true"/>
		<cfargument name="p_nomPessoa"				type="any" displayname="Nome da pessoa" required="true"/>
		<cfargument name="p_numCpfCnpj"				type="any" displayname="Número do CPF/CNPJ" required="true"/>
		<cfargument name="p_DatNascto"				type="any" displayname="Data de nascimento" required="true"/>
		
		
		<cfscript>
			//Recupera token
			v_token = CreateObject("component","#Application.PathCFC#.API_Portal_EAD").f_autentica().retorno.token;
		</cfscript>

		<cfsavecontent variable = "v_corpo">
			{
			    "Origem": "#arguments.p_origem#",
				"CodigoPessoa": #arguments.p_codigoPessoa#,
				"CodigoPessoaCorp": #arguments.p_codigoPessoaCorp#,
				"DataDeInclusao": "#arguments.p_dataDeInclusao#",
				"OrigemStatus": "#arguments.p_origemStatus#",
				"DataOrigemAtualizacao": "#arguments.p_dataOrigemAtualizacao#",
				"NomPessoa": "#arguments.p_nomPessoa#",
				"NumCpfCnpj": "#arguments.p_numCpfCnpj#",
			    "DatNascto": "#arguments.p_DatNascto#"
			}
		</cfsavecontent>

		<cfhttp url="#Application.API_BaseUnica_EAD.endpoint#/depara/gravar"
        		method="put"
        		result="resultJSON"
        		charset="utf-8">

    		<cfhttpparam type="header" name="Accept" value="application/json">
    		<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
			<cfhttpparam type="header" name="Authorization" value="jwt:#v_token#">
			<cfhttpparam type="body" name="jUser" value="#v_corpo#">
		</cfhttp>
		
		<cfscript>
			return resultJSON;
			v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
			v_retorno.Statuscode = resultJSON.Statuscode;
			//return v_retorno;
		</cfscript>
	</cffunction>



</cfoutput>
</cfcomponent>