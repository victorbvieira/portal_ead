<cfcomponent>
	<cfprocessingdirective pageEncoding='utf-8'>
	
	<cffunction name="f_consulta" returntype="query"  access="remote" >
		<cfargument name="p_id" 			type="any" displayname="Cód Ficha Tecnica"	required="no">
		<cfargument name="p_titulo"			type="any" displayname="Título"				required="no">
		
		<cfquery name="qry" datasource="#Application.datasource_ficha_tecnica#" maxrows=100>
			select campo_1 as ID,
			       to_char(campo_4, 'dd/mm/yyyy') as data_efetiva,
			       campo_7 as titulo
			  from ft_tab_ficha_tecnica_new a
			 where campo_4 = (select max(a1.campo_4) 
			                    from ft_tab_ficha_tecnica_new a1
			                   where a.campo_1 = a1.campo_1)

			 <cfif isDefined('arguments.p_id') and arguments.p_id neq ''>
			 	and campo_1 = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
			 </cfif>
			 <cfif isDefined('arguments.p_titulo') and arguments.p_titulo neq ''>
			 	and upper(campo_7) like upper(<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.p_titulo#%">)
			 </cfif>
			 order by campo_7 
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	<cffunction name="f_getByID" returntype="query"  access="remote" >
		<cfargument name="p_id" 		type="numeric" displayname="Cód Ficha Tecnica" required="yes">
		
		<cfquery name="qry" datasource="#Application.datasource_ficha_tecnica#">
			select	campo_1 as ID,
			    	to_char(campo_4, 'dd/mm/yyyy') as data_efetiva,
			    	campo_7 as titulo,
					campo_8 as gerencia,
					campo_9 as area,
					campo_10 as subarea,
					campo_11 as modalidade,
					campo_107 as cargaHoraria,
					campo_126 as descricao,
					campo_45 as preRequisitos,
					campo_149 as publicoAlvo,
					campo_33 as metodologia,
					campo_34 as progama,
					campo_148 as mercadoTrabalho,
					campo_48 as documentacao,
					campo_37 as certificacao,
					campo_82 as autorizacao,
					campo_58 as material_didatico,
					campo_101 as tags
			  from ft_tab_ficha_tecnica_new a
			 where campo_4 = (select max(a1.campo_4) 
			                    from ft_tab_ficha_tecnica_new a1
			                   where a.campo_1 = a1.campo_1)
			 	and campo_1 = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	
	<cffunction name="SincronizarToPortalEAD" access="remote" >
		<cfscript>
			v_sincronizacao = this.SincronizarToPortalEAD_Control(p_id = form.cod_ficha);
			v_destino = form.frm_retorno & '?msg=#v_msg_retorno#';
			
			location(url = "#v_destino#");
    	</cfscript>
	</cffunction>

	<cffunction name="SincronizarToPortalEAD_Control" access="remote" >
		<cfargument name="p_id"	type="numeric" displayname="Cód Ficha Tecnica" required="yes">

		<cfscript>
			cfcAPI_PortalEAD = CreateObject("component","#Application.PathCFC#.API_Portal_EAD");
			cfcDeParaModalidade = CreateObject("component","#Application.PathCFC#.depara_modalidade");
			cfcDeParaArea = CreateObject("component","#Application.PathCFC#.depara_area");

			v_dados_ft = this.f_getByID(p_id = arguments.p_id);

			// tratamento do dados de entrada
			v_sigla = 'SP';
			v_modalidade = cfcDeParaModalidade.f_getID_portalEADByID_SS(p_id_modalidade_ss = v_dados_ft.modalidade); //depara
			v_area = cfcDeParaArea.f_getID_portalEADByID_SS(p_id_area_ss = v_dados_ft.area); //depara
			v_programa = iif(v_dados_ft.progama neq '', DE(v_dados_ft.progama), DE(' ')); //campo obrigatorio
			v_certificacao = mid(v_dados_ft.certificacao, 1, 200);//campo aceita no maximo 200 caracteres
			v_descricao = iif(v_dados_ft.descricao neq '', DE(v_dados_ft.descricao), DE(' '));
			
			v_retorno_api = cfcAPI_PortalEAD.f_set_curso(p_idOrigem 	= v_dados_ft.id
														,p_sigla 		= v_sigla
														,p_modalidade 	= v_modalidade
														,p_areas 		= v_area
														,p_titulo 		= v_dados_ft.titulo
														,p_cargaHoraria = v_dados_ft.cargaHoraria
														,p_programa 	= v_programa
														,p_certificacao = v_certificacao
														,p_descricao 	= v_descricao
														);

				if (v_retorno_api.statuscode eq '201 Created'){
					v_msg_retorno_api = 'Registro criado com sucesso';
					v_statuscode_retorno_api = v_retorno_api.statuscode;
				}

			//Caso o Registro já exista chamar metodo de atualização
			if (v_retorno_api.statuscode eq '400 Bad Request' and v_retorno_api.retorno.detail eq 'O curso informado ja existe. - Id do curso duplicado para o DR'){
				v_retorno_api = cfcAPI_PortalEAD.f_put_curso(p_idOrigem 	= v_dados_ft.id
															,p_sigla 		= v_sigla
															,p_modalidade 	= v_modalidade
															,p_areas 		= v_area
															,p_titulo 		= v_dados_ft.titulo
															,p_cargaHoraria = v_dados_ft.cargaHoraria
															,p_programa 	= v_programa
															,p_certificacao = v_certificacao
															,p_descricao 	= v_descricao
															);

				if (v_retorno_api.statuscode eq '200 OK'){
					v_msg_retorno_api = 'Registro Atualizado com sucesso';
					v_statuscode_retorno_api = v_retorno_api.statuscode;
				}
			}

			if (v_retorno_api.statuscode neq '201 Created' and v_retorno_api.statuscode neq '200 OK'){
				v_msg_retorno_api = 'Erro ao processar o registro<br>';
				v_msg_retorno_api &= v_retorno_api.retorno.message & '<br>';
				v_msg_retorno_api &= #v_retorno_api.retorno.detail#;	
				v_statuscode_retorno_api = v_retorno_api.statuscode;	
			}

			v_msg_retorno = "Ficha: #form.COD_FICHA# Processada<br>";
			v_msg_retorno &= v_msg_retorno_api;

			v_retorno.mensage	= v_msg_retorno;
			v_retorno.statuscode	= v_statuscode_retorno_api;

			return v_retorno;
		</cfscript>
	</cffunction>

</cfcomponent>