<cfcomponent>
	
	<cffunction name="f_consulta" returntype="query"  access="remote" >
		<cfargument name="p_id" 			type="any" displayname="id"				required="no">
		<cfargument name="p_nome" 			type="any" displayname="Nome"			required="no">
		<cfargument name="p_ativo" 			type="any" displayname="Cadastro Ativo"	required="no">
		
		<cfquery name="qry" datasource="#Application.datasource#">
			select ID,
			       NOME,
			       GERENCIA 
			  from SNC_OPERADOR
			 where 1 = 1
			 <cfif isDefined('arguments.p_id') and arguments.p_id neq ''>
			 	and ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
			 </cfif>
			 <cfif isDefined('arguments.p_nome') and arguments.p_nome neq ''>
			 	and upper(nome) like upper(<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.p_nome#%">)
			 </cfif>
			 <cfif isDefined('arguments.p_ativo') and arguments.p_ativo neq ''>
			 	and ind_ativo = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_ativo#">
			 </cfif>
			 
			 order by NOME 
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	<cffunction name="f_getByID" returntype="query"  access="remote" >
		<cfargument name="p_id" 		type="numeric" displayname="Sistema"		required="yes">
		
		<cfquery name="qry" datasource="#Application.datasource#">
			select *
			  from SNC_OPERADOR
			 where id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	
	<cffunction name="insert" access="remote" >
		
		<cfquery name="qry" datasource="#Application.datasource#">
			insert into SNC_OPERADOR
				(id, nome, login, senha, email, matricula, gerencia, ind_ativo)
			values
			(
				snc_seq_operador.nextval,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_nome#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_login#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#Hash(form.frm_senha)#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_email#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_matricula#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_gerencia#">,
				<cfqueryparam cfsqltype="cf_sql_numeric" value="#form.frm_ind_ativo#">	
			)
		</cfquery>
		
		<cfscript>
    		v_destino = form.frm_retorno & '?msg=Operador Cadastrado';
    	</cfscript>
    	
    	<cflocation url="#v_destino#" >
	</cffunction>
	
	<cffunction name="update" access="remote" >
		
		<cfquery name="qry" datasource="#Application.datasource#">
			update SNC_OPERADOR
			   set nome 		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_nome#">,
			   	   login 		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_login#">,
			   	   <cfif form.frm_senha neq ''>
			   	   		senha  	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#Hash(form.frm_senha)#">,
			   	   </cfif>
			   	   email 		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_email#">,
			   	   matricula	= <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_matricula#">,
			   	   gerencia		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_gerencia#">,
			   	   ind_ativo 	= 	<cfqueryparam cfsqltype="cf_sql_numeric" value="#form.frm_ind_ativo#">			
			 where id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#form.frm_id#">
		</cfquery>
			
		<cfscript>
    		v_destino = form.frm_retorno & '?msg=Operador Atualizado';
    	</cfscript>
    	
    	<cflocation url="#v_destino#" >
	</cffunction>
	
	
	<cffunction name="autentica" access="remote" returntype="any" >
		<cfargument name="p_email" type="string" displayname="Email" required="yes">
		<cfargument name="p_senha" type="string" displayname="Senha" required="yes">

		<cfquery name="qry" datasource="#Application.datasource#">
			select id
			  from SNC_OPERADOR
			 where email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.p_email#">
			   and senha = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Hash(arguments.p_senha)#">
			   and ind_ativo = 1
		</cfquery>
		<cfreturn qry>
	</cffunction>


</cfcomponent>