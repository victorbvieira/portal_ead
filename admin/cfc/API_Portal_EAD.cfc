<cfcomponent>

<cfoutput>
	<cffunction name="f_autentica" returntype="any"  access="remote" >
		<!--- Função responsavel por gerar o token de autenticação no portal. --->	
		<cfsavecontent variable = "v_corpo">
			{
				"usuario": "#Application.API_portalEAD.usuario#",
				"senha": "#Application.API_portalEAD.senha#"
			}
		</cfsavecontent>

		<cfhttp url="#Application.API_portalEAD.endpoint#/autenticacao/api/auth/PORTAL_EAD/request-token"
        		method="post"
        		result="resultJSON"
        		charset="utf-8">

    		<cfhttpparam type="header" name="Accept" value="application/json">
    		<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
    		<cfhttpparam type="body" name="jUser" value="#v_corpo#">
		</cfhttp>

		<cfscript>
			v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
			v_retorno.statuscode = resultJSON.statuscode;
			return v_retorno;
		</cfscript>
	</cffunction>


	<cffunction name="f_set_curso" returntype="any"  access="remote" >
		<cfargument name="p_idOrigem"		type="any" displayname="Cód Ficha Técnica" 	required="true">
		<cfargument name="p_sigla"			type="any" displayname="DR" default="SP" 	required="true">
		<cfargument name="p_modalidade"		type="any" displayname="Modalidade"			required="true">
		<cfargument name="p_areas"			type="any" displayname="Areas"				required="true">
		<cfargument name="p_titulo"			type="any" displayname="Título"				required="true">
		<cfargument name="p_cargaHoraria" 	type="any" displayname="Carga Horária"		required="true">
		<cfargument name="p_programa" 		type="any" displayname="Programa (HTML)"	required="true">
		<cfargument name="p_certificacao" 	type="any" displayname="Certificação (HTML)"required="true">
		<cfargument name="p_descricao" 		type="any" displayname="Descrição (HTML)"	required="true">

		<!--- Cadastra um curso  --->
		<cfset v_token = "#f_autentica().retorno.token#">

		<cfsavecontent variable = "v_corpo">
			{
			    "idOrigem": #arguments.p_idOrigem#,
			    "sigla": "#arguments.p_sigla#",
			    "idModalidade": #arguments.p_modalidade#,
			    "areas": [{ "id": #arguments.p_areas#}],
			    "titulo": "#arguments.p_titulo#",
			    "cargaHoraria": "#arguments.p_cargaHoraria#",
			    "programa": "#arguments.p_programa#",
			    "certificacao": "#arguments.p_certificacao#",
			    "descricao": "#arguments.p_descricao#"
			}
		</cfsavecontent>

		<cfhttp url="#Application.API_portalEAD.endpoint#/curso/api/cursos/SP"
        		method="post"
        		result="resultJSON"
        		charset="utf-8">

    		<cfhttpparam type="header" name="Accept" value="application/json">
    		<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
    		<cfhttpparam type="header" name="Authorization" value="jwt:#v_token#">
    		<cfhttpparam type="body" name="jUser" value="#v_corpo#">
		</cfhttp>

		<cfscript>
			v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
			v_retorno.Statuscode = resultJSON.Statuscode;
			return v_retorno;
		</cfscript>
	</cffunction>

	<cffunction name="f_put_curso" returntype="any"  access="remote" >
		<cfargument name="p_idOrigem"		type="any" displayname="Cód Ficha Técnica" 	required="true">
		<cfargument name="p_sigla"			type="any" displayname="DR" default="SP" 	required="true">
		<cfargument name="p_modalidade"		type="any" displayname="Modalidade"			required="true">
		<cfargument name="p_areas"			type="any" displayname="Areas"				required="true">
		<cfargument name="p_titulo"			type="any" displayname="Título"				required="true">
		<cfargument name="p_cargaHoraria" 	type="any" displayname="Carga Horária"		required="true">
		<cfargument name="p_programa" 		type="any" displayname="Programa (HTML)"	required="true">
		<cfargument name="p_certificacao" 	type="any" displayname="Certificação (HTML)"required="true">
		<cfargument name="p_descricao" 		type="any" displayname="Descrição (HTML)"	required="true">

		<!--- Cadastra um curso  --->
		<cfset v_token = "#f_autentica().retorno.token#">

		<cfsavecontent variable = "v_corpo">
			{
			    "idOrigem": #arguments.p_idOrigem#,
			    "sigla": "#arguments.p_sigla#",
			    "idModalidade": #arguments.p_modalidade#,
			    "areas": [{ "id": #arguments.p_areas#}],
			    "titulo": "#arguments.p_titulo#",
			    "cargaHoraria": "#arguments.p_cargaHoraria#",
			    "programa": "#arguments.p_programa#",
			    "certificacao": "#arguments.p_certificacao#",
			    "descricao": "#arguments.p_descricao#"
			}
		</cfsavecontent>

		<cfhttp url="#Application.API_portalEAD.endpoint#/curso/api/cursos/SP/#arguments.p_idOrigem#"
        		method="put"
        		result="resultJSON"
        		charset="utf-8">

    		<cfhttpparam type="header" name="Accept" value="application/json">
    		<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
    		<cfhttpparam type="header" name="Authorization" value="jwt:#v_token#">
    		<cfhttpparam type="body" name="jUser" value="#v_corpo#">
		</cfhttp>

		<cfscript>
			v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
			v_retorno.Statuscode = resultJSON.Statuscode;
			return v_retorno;
		</cfscript>
	</cffunction>	


	<cffunction name="f_set_turma" returntype="any"  access="remote" >
		<cfargument name="p_idCursoOrigem"		type="any" required="true" displayname="Cód Ficha Técnica">
		<cfargument name="p_idTurmaOrigem"		type="any" required="true" displayname="Identificador único da turma" default="SP">
		<cfargument name="p_dataInicio"			type="any" required="false" displayname="Define quando a turma inicia no curso">
		<cfargument name="p_dataTermino"		type="any" required="false" displayname="Define quando a turma termina no curso">
		<cfargument name="p_dataPublicacao"		type="any" required="true" displayname="Define a data de início para realização da inscrição">
		<cfargument name="p_dataExpiracao"		type="any" required="true" displayname="Define a data de termino para realização da inscrição">
		<cfargument name="p_precoNormal"		type="any" required="true" displayname="Representa o valor da parcela do curso">
		<cfargument name="p_precoPromocional"	type="any" required="false" displayname="Representa o valor do curso com desconto">
		<cfargument name="p_investimento"		type="any" required="true" displayname="Texto com informações de investimentos e condições de pagamento">
		<cfargument name="p_formasPagamento"	type="any" required="true" displayname="Texto curto com informações sobre a forma de pagamento">
		<cfargument name="p_quantidadeParcelas"	type="any" required="false" displayname="Campo informativo que será usado para representar quantidade de parcelas do valor do curso(promocional ou normal)">
		<cfargument name="p_polos"				type="any" required="false" displayname="CNPJ dos polos que estão disponíveis no processo de inscrição">

		<!--- Cadastra um curso  --->
		<cfset v_token = "#f_autentica().retorno.token#">

		<cfsavecontent variable = "v_corpo">
			{
			    "idCursoOrigem": #arguments.p_idCursoOrigem#,
				"idTurmaOrigem": "#arguments.p_idTurmaOrigem#",
				"dataInicio": "#arguments.p_dataInicio#",
				"dataTermino": "#arguments.p_dataInicio#",
				"dataPublicacao": "#arguments.p_dataPublicacao#",
				"dataExpiracao": "#arguments.p_dataExpiracao#",
				"precoNormal": "#arguments.p_precoNormal#",
				"precoPromocional": "#arguments.p_precoPromocional#",
			    "investimento": "#arguments.p_investimento#",
				"formasPagamento": "#arguments.p_formasPagamento#",
				"quantidadeParcelas": "#arguments.p_quantidadeParcelas#"
			}
		</cfsavecontent>

		
		<cfhttp url="#Application.API_portalEAD.endpoint#/curso/api/turmas/SP"
        		method="post"
        		result="resultJSON"
        		charset="utf-8">

    		<cfhttpparam type="header" name="Accept" value="application/json">
    		<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
    		<cfhttpparam type="header" name="Authorization" value="jwt:#v_token#">
    		<cfhttpparam type="body" name="jUser" value="#v_corpo#">
		</cfhttp>
		
		<cfscript>
			v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
			v_retorno.Statuscode = resultJSON.Statuscode;
			return v_retorno;
		</cfscript>
	</cffunction>


	<cffunction name="f_put_turma" returntype="any"  access="remote" >
		<cfargument name="p_idCursoOrigem"		type="any" required="true" displayname="Cód Ficha Técnica">
		<cfargument name="p_idTurmaOrigem"		type="any" required="true" displayname="Identificador único da turma" default="SP">
		<cfargument name="p_dataInicio"			type="any" required="true" displayname="Define quando a turma inicia no curso">
		<cfargument name="p_dataTermino"		type="any" required="true" displayname="Define quando a turma termina no curso">
		<cfargument name="p_dataPublicacao"		type="any" required="true" displayname="Define a data de início para realização da inscrição">
		<cfargument name="p_dataExpiracao"		type="any" required="true" displayname="Define a data de termino para realização da inscrição">
		<cfargument name="p_precoNormal"		type="any" required="true" displayname="Representa o valor da parcela do curso">
		<cfargument name="p_precoPromocional"	type="any" required="true" displayname="Representa o valor do curso com desconto">
		<cfargument name="p_investimento"		type="any" required="true" displayname="Texto com informações de investimentos e condições de pagamento">
		<cfargument name="p_formasPagamento"	type="any" required="true" displayname="Texto curto com informações sobre a forma de pagamento">
		<cfargument name="p_quantidadeParcelas"	type="any" required="true" displayname="Campo informativo que será usado para representar quantidade de parcelas do valor do curso(promocional ou normal)">

		<!--- Cadastra um curso  --->
		<cfset v_token = "#f_autentica().retorno.token#">

		<cfsavecontent variable = "v_corpo">
			{
			    "idCursoOrigem": #arguments.p_idCursoOrigem#,
				"idTurmaOrigem": "#arguments.p_idTurmaOrigem#",
				"dataInicio": "#arguments.p_dataInicio#",
				"dataTermino": "#arguments.p_dataInicio#",
				"dataPublicacao": "#arguments.p_dataPublicacao#",
				"dataExpiracao": "#arguments.p_dataExpiracao#",
				"precoNormal": "#arguments.p_precoNormal#",
				"precoPromocional": "#arguments.p_precoPromocional#",
			    "investimento": "#arguments.p_investimento#",
				"formasPagamento": "#arguments.p_formasPagamento#",
				"quantidadeParcelas": #arguments.p_quantidadeParcelas#
			}
		</cfsavecontent>

		<cfhttp url="#Application.API_portalEAD.endpoint#/curso/api/turmas/SP/#arguments.p_idTurmaOrigem#"
        		method="put"
        		result="resultJSON"
        		charset="utf-8">

    		<cfhttpparam type="header" name="Accept" value="application/json">
    		<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
    		<cfhttpparam type="header" name="Authorization" value="jwt:#v_token#">
    		<cfhttpparam type="body" name="jUser" value="#v_corpo#">
		</cfhttp>

		<cfscript>
			v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
			v_retorno.Statuscode = resultJSON.Statuscode;
			return v_retorno;
		</cfscript>
	</cffunction>	





</cfoutput>
</cfcomponent>