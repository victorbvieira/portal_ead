<cfcomponent>

    <cfoutput>
    
        <cffunction name="f_get_intencao_compra" returntype="any"  access="remote" >
            <cfargument name="p_idIntencaoCompra" type="any" required="true" displayname="Código único que identifica a intenção de compra.">
    
            <cfscript>
                //Recupera token
                v_token = CreateObject("component","#Application.PathCFC#.API_Portal_EAD").f_autentica().retorno.token;
            </cfscript>
    
            <cfhttp url="#Application.API_portalEAD.endpoint#/ecommerce/api/intencao-compra/#arguments.p_idIntencaoCompra#"
                    method="get"
                    result="resultJSON"
                    charset="utf-8">
    
                <cfhttpparam type="header" name="Accept" value="application/json">
                <cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
                <cfhttpparam type="header" name="Authorization" value="jwt:#v_token#">
            </cfhttp>
    
            <cfscript>
                v_retorno.retorno = "#deserializeJSON(resultJSON.filecontent)#";
                v_retorno.Statuscode = resultJSON.Statuscode;
                return v_retorno;
            </cfscript>
        </cffunction>	
     
    
    
    
        
    
    
    
    
    
    </cfoutput>
    </cfcomponent>