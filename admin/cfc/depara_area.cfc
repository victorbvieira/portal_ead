<cfcomponent>
	
	<cffunction name="f_consulta" returntype="query"  access="remote" >
		<cfargument name="p_id" 			type="any" displayname="id"				required="no">
		<cfargument name="p_id_area" 			type="any" displayname="id_area"			required="no">
		<cfargument name="p_area" 			type="any" displayname="Cadastro Ativo"	required="no">
		<cfargument name="p_id_ss" 			type="any" displayname="Id área no SS"	required="no">
		
		<cfquery name="qry" datasource="#Application.datasource#">
			select ID,
			       ID_AREA,
				   AREA,
				   ID_AREA_SS
			  from DEPARA_PORTAL_EAD_AREA
			 where ind_ativo = 'A'
			 <cfif isDefined('arguments.p_id') and arguments.p_id neq ''>
			 	and ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
			 </cfif>
			 <cfif isDefined('arguments.p_id_area') and arguments.p_id_area neq ''>
			 	and id_area = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id_area#">
			 </cfif>
			 <cfif isDefined('arguments.p_area') and arguments.p_area neq ''>
			 	and upper(area) like upper(<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.p_area#%">)
			 </cfif>
			 <cfif isDefined('arguments.p_id_ss') and arguments.p_id_ss neq ''>
			 	and id_area_ss = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id_ss#">
			 </cfif>
			 
			 order by area 
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	<cffunction name="f_getByID" returntype="query"  access="remote" >
		<cfargument name="p_id" 		type="numeric" displayname="Sistema"		required="yes">
		
		<cfquery name="qry" datasource="#Application.datasource#">
			select *
			  from DEPARA_PORTAL_EAD_AREA
			 where id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id#">
			   and ind_ativo = 'A'
		</cfquery>
		
		<cfreturn qry>
	</cffunction>
	
	
	<cffunction name="insert" access="remote" >
		
		<cfquery name="qry" datasource="#Application.datasource#">
			insert into DEPARA_PORTAL_EAD_AREA
				(id, id_area, area, id_area_ss)
			values
			(
				SEQ_DEPARA_AREA.nextval,
				<cfqueryparam cfsqltype="cf_sql_integer" value="#form.frm_id_area#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_area#">,
				<cfqueryparam cfsqltype="cf_sql_integer" value="#form.frm_id_ss#">
			)
		</cfquery>
		
		<cfscript>
    		v_destino = form.frm_retorno & '?msg=De/Para Cadastrado';
    	</cfscript>
    	
    	<cflocation url="#v_destino#" >
	</cffunction>
	
	<cffunction name="update" access="remote" >
		
		<cfquery name="qry" datasource="#Application.datasource#">
			update DEPARA_PORTAL_EAD_AREA
			   set id_area 		= <cfqueryparam cfsqltype="cf_sql_integer" value="#form.frm_id_area#">,
			   	   area 		= <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.frm_area#">,
			   	   id_area_ss	= <cfqueryparam cfsqltype="cf_sql_integer" value="#form.frm_id_ss#">,
				   dt_alt = sysdate
			 where id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#form.frm_id#">
		</cfquery>
			
		<cfscript>
    		v_destino = form.frm_retorno & '?msg=De/Para Atualizado';
    	</cfscript>
    	
    	<cflocation url="#v_destino#" >
	</cffunction>

	<cffunction name="delete" access="remote" >
		
		<cfquery name="qry" datasource="#Application.datasource#">
			update DEPARA_PORTAL_EAD_AREA
			   set ind_ativo = 'I',
			   	   dt_alt = sysdate
			 where id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#form.frm_id#">
		</cfquery>
			
		<cfscript>
    		v_destino = form.frm_retorno & '?msg=De/Para Indativado';
    	</cfscript>
    	
    	<cflocation url="#v_destino#" >
	</cffunction>

	<cffunction name="f_getID_portalEADByID_SS" returntype="any"  access="remote" >
		<cfargument name="p_id_area_ss" 		type="numeric" displayname="id modalidade SS"		required="yes">
		
		<cfquery name="qry" datasource="#Application.datasource#">
			select id_area
			  from DEPARA_PORTAL_EAD_AREA
			 where id_area_ss = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.p_id_area_ss#">
			   and ind_ativo = 'A'
		</cfquery>
		
		<cfreturn qry.id_area>
	</cffunction>
</cfcomponent>