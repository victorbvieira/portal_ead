<cfscript>
	switch(v_form_action) {
		case "insert":
			v_config_input = '';
			break;
		case "update":
			v_config_input = '';
			break;
		case "view/delete":
			v_config_input = 'readonly style="border: 0px solid white;"';
			break;
	}
</cfscript>


<cfoutput>
	
<!--- Informações de Cadastro --->	
<div class="row">	
	<div class="col-sm-12">
		<section class="panel panel-default">
			<header class="panel-heading">
				<i class="fa fa-list-alt"></i> Informações de Cadastro
				<span class="tools pull-right">
				<a href="javascript:;" class="fa fa-chevron-down"></a>
				<!--- <a href="javascript:;" class="fa fa-times"></a> Fecha --->
			</span>
		</header>
		
		<div class="panel-body">
			
			<table border="0" width="100%">
				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;ID: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete' or v_form_action eq 'insert'>
							<div class="panel-body">#v_frm_id#</div>
						<cfelse>
							<input type="text" name="frm_id2" value="#v_frm_id#" class="form-control input-sm" #v_config_input# readonly/>
						</cfif>
					</td>
				</tr>
				
				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Nome: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_nome#</div>
						<cfelse>
							<input type="text" name="frm_nome" value="#v_frm_nome#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
				
				<tr height="40px" style="background-color: ##eaeaea" >
					<td width="200px">
						<label class="control-label">&nbsp;Login: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_login#</div>
						<cfelse>
							<input type="text" name="frm_login" value="#v_frm_login#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
				
				<tr height="40px" >
					<td width="200px">
						<label class="control-label">&nbsp;Senha: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_senha#</div>
						<cfelse>
							<input type="password" name="frm_senha" value="#v_frm_senha#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
				
				<tr height="40px" style="background-color: ##eaeaea">
					<td width="200px">
						<label class="control-label">&nbsp;Email: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_email#</div>
						<cfelse>
							<input type="text" name="frm_email" value="#v_frm_email#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
				
				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Matricula: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_matricula#</div>
						<cfelse>
							<input type="text" name="frm_matricula" value="#v_frm_matricula#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
				
				<tr height="40px" style="background-color: ##eaeaea">
					<td width="200px">
						<label class="control-label">&nbsp;Gerência: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">#v_frm_gerencia#</div>
						<cfelse>
							<input type="text" name="frm_gerencia" value="#v_frm_gerencia#" class="form-control input-sm" #v_config_input# />
						</cfif>
					</td>
				</tr>
				
				<tr height="40px">
					<td width="200px">
						<label class="control-label">&nbsp;Ativo: </label>
					</td>
					<td>
						<cfif v_form_action eq 'view/delete'>
							<div class="panel-body">
								<cfif v_frm_ind_ativo eq '1' or v_frm_ind_ativo eq ''>
									SIM
								<cfelse>
									NÃO
								</cfif>
							</div>
						<cfelse>
							<input type="radio" name="frm_ind_ativo" value="1" #IIF( v_frm_ind_ativo eq 1 or v_frm_ind_ativo eq '', DE("checked"), DE("") )#>SIM
							&nbsp;&nbsp;&nbsp;
							<input type="radio" name="frm_ind_ativo" value="0" #IIF( v_frm_ind_ativo eq 0, DE("checked"), DE("") )#>NÃO
						</cfif>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
</cfoutput>