<cfprocessingdirective pageencoding="utf-8" >
<cfinclude template="./../templates/check_autenticao.cfm" >	
<cfscript>
	v_area = 'operadores';
	v_pagina = 'operadores';

	//Controle Barra Ações
	url.BtnNovo		= false;
	url.BtnEditar	= true;
	url.BtnExcluir	= false;
	
	cfcPagina = CreateObject("component","#Application.PathCFC#.#v_area#");

	v_dados = cfcPagina.f_getByID(p_id = url.id);
		
	// Setup dos valores do form
	v_form_action = 'view/delete'; // valores: insert, view/delete, update
	
	//Setando Variaveis
	session.id			= v_dados.id;
	v_frm_id 			= v_dados.id;
	v_frm_nome	 		= v_dados.nome;
	v_frm_login			= v_dados.login;
	v_frm_senha			= '';
	v_frm_email 		= v_dados.email;
	v_frm_matricula 	= v_dados.matricula;
	v_frm_gerencia	 	= v_dados.gerencia;
	v_frm_ind_ativo		= v_dados.ind_ativo;
	
</cfscript>


<!DOCTYPE html>
<html lang="pt">

	<cfinclude template="../templates/page_head.cfm" >

<body style="">
<cfoutput>
<section id="container">
	<!--- header --->
	<cfinclude template="../templates/header.cfm" >

	<!--- Menu Lateral --->
	<cfinclude template="../templates/menu.cfm" >

	<!--- Conteudo Principal --->
    <section id="main-content" class="">
		<section class="wrapper">
        
        <!--- Sub Header --->
        <div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb small">
                	<li><a href="#Application.AdminPathLogico#/home.cfm"><i class="fa fa-home"></i> Home</a></li>
                	<li>Administração</li>
                    <li><a href="#Application.AdminPathLogico#/operadores/operadores.cfm">Operadores</a></li>
                    <li class="active">View</li>
				</ul>
                    
			</div>
		</div>
         
        
        <cfinclude template="./../templates/barra_acoes.cfm" > 
         
		<cfinclude template="./#v_pagina#_form.cfm">

					
		<br>
		
		<div class="panel-footer">
			<a href="#v_pagina#.cfm"><button type="button" class="btn "><i class="fa fa-remove"></i> Voltar</button></a>
		</div>

		</section>
	</section>
</section>

<div class="clearfix"> </div>
	
<cfinclude template="./../templates/rodape.cfm" >

</cfoutput>			

</body>
</html>

<cfinclude template="../templates/footer_page.cfm" >

