component extends="testbox.system.BaseSpec"{
	pageencoding "utf-8";

	function run(){
		describe( "Teste API Portal EAD (API_Portal_EAD)", function() {

			cfc = CreateObject("component","#Application.PathCFC#.API_Portal_EAD");

			it( "Erro com o metodo de f_autentica", function() {
				var oTest = cfc.f_autentica();
				expect( oTest.statuscode ).toBe('200 OK');
			} );
		

			it( "Erro com o metodo de cadastra curso (f_set_curso)", function() {
				var oTest = cfc.f_set_curso( p_idOrigem 	= 33
											,p_sigla 		= 'SP'
											,p_modalidade 	= 6
											,p_areas 		= 18
											,p_titulo 		= 'teste titulo'
											,p_cargaHoraria = '40'
											,p_programa 	= 'Teste programa'
											,p_certificacao = 'teste certificacao'
											,p_descricao 	= 'teste descricao');
				// Como a API retorna erro se é enviando um cadastro já existente, estamos testando o valor do erro.
				expect( oTest.statuscode ).toBe('400 Bad Request');
				expect( oTest.retorno.message ).toBe('Regra de negócio violada');
				expect( oTest.retorno.detail ).toBe('O curso informado ja existe. - Id do curso duplicado para o DR');
			} );


			it( "Erro com o metodo de atualiza curso (f_put_curso)", function() {
				var oTest = cfc.f_put_curso( p_idOrigem 	= 33
											,p_sigla 		= 'SP'
											,p_modalidade 	= 6
											,p_areas 		= 18
											,p_titulo 		= 'teste titulo'
											,p_cargaHoraria = '40'
											,p_programa 	= 'Teste programa'
											,p_certificacao = 'teste certificacao'
											,p_descricao 	= 'teste descricao');
				expect( oTest.statuscode ).toBe('200 OK');
			} );


			it( "Erro com o metodo de cadastra de Turma (f_set_turma)", function() {
				var oTest = cfc.f_set_turma( p_idCursoOrigem 		= 33
											,p_idTurmaOrigem 		= '9900101157'
											,p_dataInicio			= '2018-08-06T08:00:00.238Z'
											,p_dataTermino			= '2018-12-17T08:00:00.238Z'
											,p_dataPublicacao 		= '2018-05-08T08:00:00.238Z'
											,p_dataExpiracao 		= '2018-08-06T08:00:00.238Z'
											,p_precoNormal 			= '1884.00'
											,p_precoPromocional		= '1884.00'
											,p_investimento 		= 'teste investimento'
											,p_formasPagamento 		= 'teste Pagamento'
											,p_quantidadeParcelas 	= 6);
				//expect( oTest.statuscode ).toBe('201 Created');
				// Como a API retorna erro se é enviando um cadastro já existente, estamos testando o valor do erro.
				expect( oTest.statuscode ).toBe('400 Bad Request');
				expect( oTest.retorno.message ).toBe('Regra de negócio violada');
				expect( oTest.retorno.detail ).toBe('A turma informada ja existe. - Id de origem da turma duplicado para o Curso');
			} );


			it( "Erro com o metodo de atualiza cadastro de Turma (f_put_turma)", function() {
				var oTest = cfc.f_put_turma( p_idCursoOrigem 		= 33
											,p_idTurmaOrigem 		= '9900101157'
											,p_dataInicio			= '2018-08-06T08:00:00.238Z'
											,p_dataTermino			= '2018-12-17T08:00:00.238Z'
											,p_dataPublicacao 		= '2018-05-08T08:00:00.238Z'
											,p_dataExpiracao 		= '2018-08-06T08:00:00.238Z'
											,p_precoNormal 			= '1884.00'
											,p_precoPromocional		= '1884.00'
											,p_investimento 		= 'teste investimento'
											,p_formasPagamento 		= 'teste Pagamento'
											,p_quantidadeParcelas 	= 6);
				expect( oTest.statuscode ).toBe('200 OK');
			} );

		} );
			
	}

}