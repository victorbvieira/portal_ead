component extends="testbox.system.BaseSpec"{
	pageencoding "utf-8";

	function run(){
		describe( "Teste API BU EAD (API_BU_EAD)", function() {

			cfc = CreateObject("component","#Application.PathCFC#.API_BU_EAD");

			it( "Erro com o metodo de f_get_pessoa", function() {
				var oTest = cfc.f_get_pessoa(p_origem = 'SP'
											,p_codPessoaCorp = 1140129338);
				expect( oTest.statuscode ).toBe('200 OK');
			} );

			it( "Erro com o metodo de f_put_pessoa", function() {
				var oTest = cfc.f_put_pessoa(p_codOrigemStatus = 'A'
											 ,p_dscOrigem = 'SP'
											 ,p_codPessoa = '114015076'
											 ,p_indTipoPessoa = 'F'
											 ,p_nomPessoa = 'Giane Simone Passos Passarelli'
											 ,p_numCpfCnpj = '94686564972'
											 ,p_datNascto = '1973-10-22T00:00:00.000Z'
											 ,p_datOrigemAtual = '2018-05-01T08:00:00.238Z'
											 ,p_datOrigemCadtro = '2000-01-01T08:00:00.238Z'
											 ,p_DscEmail = 'gianepassos@hotmail.com'
											 ,p_DscEmailPartcl = 'gianepassos@hotmail.com');
				expect( oTest.statuscode ).toBe('200 OK');
			} );

			it( "Erro com o metodo de f_depara_get", function() {
				var oTest = cfc.f_get_depara(p_origem = 'SP'
											,p_codigoPessoa = '114015076');
				expect( oTest.statuscode ).toBe('200 OK');
			} );

			it( "Erro com o metodo de f_depara_put", function() {
				var oTest = cfc.f_put_depara(p_origem = 'SP'
											,p_codigoPessoa = '114015076'
											,p_codigoPessoaCorp = '44233'
											,p_dataDeInclusao = '2018-05-01T08:00:00.238Z'
											,p_origemStatus = 'A'
											,p_dataOrigemAtualizacao = '2018-05-01T08:00:00.238Z'
											,p_nomPessoa = 'Giane Simone Passos Passarelli'
											,p_numCpfCnpj = '94686564972'
											,p_DatNascto = '1973-10-22T00:00:00.000Z');

				expect( oTest.statuscode ).toBe('200 OK');
			} );

		} );
			
	}

}