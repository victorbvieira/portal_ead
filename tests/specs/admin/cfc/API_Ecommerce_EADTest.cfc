component extends="testbox.system.BaseSpec"{
	pageencoding "utf-8";

	function run(){
		describe( "Teste API Ecommerce EAD (API_Ecommerce_EAD)", function() {

			cfc = CreateObject("component","#Application.PathCFC#.API_Ecommerce_EAD");


			it( "Erro com o metodo de recupera intenção de compra (f_get_intencao_compra)", function() {
				var oTest = cfc.f_get_intencao_compra( p_idIntencaoCompra = 1);
				expect( oTest.statuscode ).toBe('200 OK');
			} );
		
		} );
			
	}

}