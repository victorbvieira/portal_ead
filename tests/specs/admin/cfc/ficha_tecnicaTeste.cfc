component extends="testbox.system.BaseSpec"{
	pageencoding "utf-8";

	function run(){
		describe( "Teste classe Ficha Técnica (ficha_tecnica)", function() {

			cfc = CreateObject("component","#Application.PathCFC#.ficha_tecnica");

			it( "Erro com o metodo de f_consulta sem parametros", function() {
				var oTest = cfc.f_consulta();
                expect( oTest ).toBeTypeOf( 'query' );
            } );
            
            it( "Erro com o metodo de f_consulta recuperando um id especifico", function() {
				var oTest = cfc.f_consulta(p_id = 8376);
                expect( oTest ).toBeTypeOf( 'query' );
                expect( oTest.id ).toBe('8376');
            } );
            
            it( "Erro com o metodo de f_getByID recuperando um id especifico", function() {
				var oTest = cfc.f_getByID(p_id = 8376);
                expect( oTest ).toBeTypeOf( 'query' );
                expect( oTest.id ).toBe('8376');
			} );

		} );
			
	}

}