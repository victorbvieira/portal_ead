component extends="testbox.system.BaseSpec"{
	pageencoding "utf-8";

	function run(){
		describe( "Teste do processo de integração entre o portal ead e a ficha de inscrição", function() {

			//cfc = CreateObject("component","#Application.PathCFC#.API_BU_EAD");

			it( "Consulta intenção de compra", function() {
				var oTest = cfc.f_get_pessoa(p_origem = 'SP' ,p_codPessoaCorp = 1140129338);
				oTest.statuscode = 2;
				expect( oTest.statuscode ).toBe('200 OK');
			} );

			it( "Consulta/Cria Pessoa BU SP", function() {
				var oTest = cfc.f_get_pessoa(p_origem = 'SP' ,p_codPessoaCorp = 1140129338);
				oTest.statuscode = 2;
				expect( oTest.statuscode ).toBe('200 OK');
			} );

			it( "Consulta/Cria DE/PARA (Processo Seletivo) BU SP", function() {
				var oTest = cfc.f_get_pessoa(p_origem = 'SP' ,p_codPessoaCorp = 1140129338);
				oTest.statuscode = 2;
				expect( oTest.statuscode ).toBe('200 OK');
			} );

			it( "Consulta/Cria DE/PARA (Processo Seletivo) BU EAD", function() {
				var oTest = cfc.f_get_pessoa(p_origem = 'SP' ,p_codPessoaCorp = 1140129338);
				oTest.statuscode = 2;
				expect( oTest.statuscode ).toBe('200 OK');
			} );

		} );
			
	}

}