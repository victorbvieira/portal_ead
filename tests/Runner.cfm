<cfsetting showdebugoutput="false">

<!--- Executa todos os testes na pasta 'specs' e retornar o simples relatorio padrão --->
<cfparam name="url.reporter"	default="Simple">
<cfparam name="url.directory"	default="tests.specs">
<cfparam name="url.recurse"		default="true"	type="boolean">
<cfparam name="url.bundles"		default="">
<cfparam name="url.labels"		default="">

<!--- incluir o TestBox HTML Runner --->
<cfinclude template="/testbox/system/runners/HTMLRunner.cfm">
